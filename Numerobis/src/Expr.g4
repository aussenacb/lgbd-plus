/**
 * Define a grammar called Hello
 */
grammar Expr;

prog :
  (instruction ';')* 'fin' ;

//=====================
// Instructions

instruction :
  type_dec 
  | expression
  | operateur
  ;

//=====================
// Types

type_dec :
  DEFINIR TYPE IDENT type_def
  ;

type_def :
  type_de_base
  | type_construit
  | type_non_scalaire 
  ;

type_de_base :
  type_primitif 
  | type_derive
  // 
  // � un type d�riv� a n�cessaire une type_id de r�f�rence ainsi qu'une contrainte �
  ;

type_primitif : // �galement appel� domaine (domain) ou type-racine (root type)
  ordre representation 
  // � la contrainte permet de d�terminer une sous-ensemble du langage d�fini par le repr�sentation �
;

ordre :
  (NON)? ORDONNE
  ;
  
representation :
  expression_rationnelle (contrainte)?
  // l'ensemble des valeurs d'un type primitif est l'ensemble phases 
  // (satisfaisant l'�ventuelle contrainte) appartenant au langage d�crit par exp_rationnelle
  ;
  
type_derive : // �galement appel� sous-type
  '(' type_id ')' contrainte
  // � l'ensemble des valeurs d'un type d�riv� est l'ensemble des valeurs de type_id satisfaisant la contrainte �
  ;

type_id :
  // Un identifiant de type d�fini dans la port�e.
  IDENT
  ;

type_construit :
  type_ensemble | type_intervalle
  ;
type_ensemble :
  ENSEMBLE type_id
  ;
type_intervalle :
  INTERVALLE type_ordonne
  ;
type_ordonne :
  type_id
  // R�f�rence � un type dont l'ensemble des valeurs est muni d'une relation d'ordre total.
  ;
  
  

type_non_scalaire :
  (type_tuple | type_relation) (contrainte)? (predicat)?
  // Il n'y a pas de type_base car les instructions du LDD sont sp�cifiquement destin�es � g�rer
  // l'unique variable de ce type par l'entremise du catalogue.
  ;
type_tuple :
  TUPLE entete_tuple
  ;
type_relation :
  RELATION entete_tuple 
  ;
entete_tuple :
  '{' (attribut_dec (', ' attribut_dec)*)? '}'
  ;
attribut_dec :
  attribut_id ':' type_id
  ;
attribut_id :
  // Un identifiant d'attribut d�fini dans la port�e.
  IDENT
  ;
predicat : 
// Pr�dicat pour les relations
  PREDICAT '{' STRING (STRING)* '}' ;  


//=====================
// Contraintes

contrainte :
  CONTRAINTE contrainte_liste
  ;
contrainte_liste :
  '{' (contrainte_dec (';' contrainte_dec)*)? '}'
  // La contrainte globale est obtenue par la conjonction des contraintes_dec.
  ;
contrainte_dec :
  (contrainte_id ':' )? expression_booleenne 
  // L'identifiant permet de d�finir, d'ind�finir ou de changer la contraint associ�e ;
  // il peut (doit?) aussi �tre utiliser lors du signalement du non-respect de la contrainte.
  ;
contrainte_id :
  // Un identifiant de contrainte d�fini dans la port�e.
  IDENT
  ;

 
//=====================
// � d�finir

expression_booleenne :
  expression 
  //expression dont la valeur appartient au type Booleen 
  ;
  
expression_rationnelle : 
  STRING 
  // la valeur est un texte repr�sentant une expression rationnelle
  ;
  

expression :
  valeur 
  | fonction_appel_calcul
  | calcul 
  ;

calcul :
  identifiant_calcul '(' expression ', ' expression ')'
  // l'identifiant prend la valeur expression0 au sein de expression1
  ;
  
 identifiant_calcul : 
 	id_calcul_scalaire
 	| id_calcul_non_scalaire
 	;
 	
 id_calcul_scalaire :
   '<'
   | '>'
   | '=='
   | '='
   | '>='
   | '<='
   | '!='
   | 'modulo'
   | '||'
   | '&&'
   | '+'
   ;
   
id_calcul_non_scalaire :
	 (NON)? 'IN'
   | ('DISJOINT')? 'UNION'
   | 'JOIN' 
   | 'RESTRICTION'
   | 'PROJECTION' 
   | 'DIFFERENCE'
   | IDENT
   // tel que d�fini dans le compilateur
   // l'ident d�fini alors les calculs op�rable par le langage (substring, count, etc...)
   ;

fonction_appel_calcul :
  fonction_id (expression)* 
  ;

fonction_id_calcul :
	IDENT
	;

valeur :
  valeur_scalaire 
  | valeur_non_scalaire 
  ;
  
valeur_scalaire :
  repr�sentation_effective
  // la repr�sentation effective doit �tre conforme � la repr�sentation prescrite par le type 
  ;

repr�sentation_effective :
  IDENT
  | INTEGER
  | FLOAT
  | STRING
  // selon le langage concret
  ;
 
valeur_non_scalaire :
  valeur_tuple 
  | valeur_relation 
  ;
  
valeur_tuple :
  TUPLE '{' valeur_liste_att '}'
  | '*' 
  // une liste de valeur ou bien la totalit� avec *
  ;
  
valeur_liste_att :
  (attribut_id (TELQUE expression)? (', ' attribut_id (TELQUE expression)?)*)? 
  ;
  
valeur_relation :
  ('(')? RELATION valeur_liste_rel (')')?
  ;
  
valeur_liste_rel :
  valeur_liste_att (', ' valeur_liste_att)*
  ;


//============
// Operations 

operateur :
	operateur_scalaire //les fonctions
	| operateur_non_scalaire //les transactions
	;

operateur_scalaire : 
	fonction_dec
	| fonction_appel
	//dont le comportement n'affecte pas la base de donn�es
	;

operateur_non_scalaire : 
	transaction_dec
	| transaction_appel
	| assertion_dec
	// dont le comportement affecte la base de donn�es
	;

bloc : 
	'BEGIN' ((expression | ifstmt | boucle ) ';' )* 'END'
	;
	
ifstmt :
	IF '(' expression_booleenne ')' THEN bloc 
	(ELSEIF '(' expression_booleenne ')' THEN bloc )*
	(ELSE THEN bloc)?
	;


param_dec :
  param_id ':' type_id
  ;
  
param_id :
  // Un identifiant de parametre d�fini dans la port�e.
  IDENT
  ;


boucle :
	FOR '(' expression_booleenne ')' bloc  
	;

fonction_dec :
	DEFINIR FONCTION fonction_id '(' (param_dec (', ' param_dec )*)? ')' 
	RETURNS type_id
	bloc
	; 
	
	
fonction_id :
	IDENT
	//l'identifiant d'une fonction
	;

fonction_appel :
	fonction_id '(' (repr�sentation_effective (', ' repr�sentation_effective)* )? ')'
	//qui renvoie un �l�ment et n'alt�re pas la base de donn�es.
	// avec une repr�sentation effective des param�tres
	;	

transaction_dec :
	DEFINIR TRANSACTION transaction_id '(' (param_dec (', ' param_dec )*)? ')' 
	bloc
	;
	
transaction_appel :
	transaction_id '(' (repr�sentation_effective (', ' repr�sentation_effective)* )? ')'
	//qui ne renvoie pas d'�l�ment mais alt�re la base de donn�es.
	// avec une repr�sentation effective des param�tres
	;
	
transaction_id :
	IDENT
	// identifiant de la transation
	;

assertion_dec :
	DEFINIR ASSERTION assertion_id (predicat)?
	bloc
	// bloc qui r�sulte d'une expression bool�ene
	;

assertion_id : 
	IDENT;
	
//=====================
// Mots r�serv�s
IF : 'IF' | 'si' ;
ELSEIF : 'ELSE IF' | 'sinon si' ;
ELSE : 'ELSE' | 'sinon' ;
THEN : 'THEN' | 'alors' ;
FOR : 'FOR' | 'boucle' ;

FONCTION : 'FUNCTION' | 'fonction' ;
TRANSACTION : 'TRANSACTION' | 'transaction' ;


RETURNS : 'RETURNS' | 'retourne' ;

DEFINIR : 'CREATE' | 'definir' ;
INDEFINIR : 'DROP' | 'indefinir';
CHANGER : 'ALTER' | 'changer';
NON : 'NOT' | 'non' ;
ORDONNE : 'ORDERED' | 'ordonne' ;
TYPE : 'TYPE' | 'type' ;
ENSEMBLE : 'SET' | 'ensemble' ;
INTERVALLE : 'INTERVAL' | 'interval';
TUPLE : 'TUPLE' | 'tuple' ;
RELATION : 'RELATION' | 'relation' ;
BASE : 'BASE' | 'base' ;
CONTRAINTE : 'CONSTRAINT' | 'contrainte' ;
DANS : 'FROM' | 'dans' ;
TELQUE: 'WHERE' | 'tel que' ;
PREDICAT : 'PREDICAT' | 'predicat' ;
ASSERTION : 'ASSERTION' | 'assertion' ;



// ====================
// Lexique de base - pourra �tre �tendu au besoin
//
IDENT :
  IDENT_SIMPLE | IDENT_DELIM
  ;
STRING :
  STRING_UNI | STRING_MULTI
  ;
CHARACTER :
  STRING_CAR
  ;
INTEGER :
                    DEC_DIGITS
    | '0' ('b'|'B') BIN_DIGITS
    | '0' ('c'|'C') OCT_DIGITS
    | '0' ('x'|'X') HEX_DIGITS
    ;
FLOAT :
      DEC_DIGITS '.' DEC_DIGITS  EXPONENT?
    | DEC_DIGITS                 EXPONENT
    ;

// ====================
// Conventions relatives aux espaces, fins de ligne et commentaires
//
ESPACES :
  (' ' | '\t' | '\n' | '\r' | '\f' )+ -> skip
  ;
COMMENTAIRE_LIGNE :
  '//' ~('\n'|'\r')* ('\n'|'\r') -> skip
  ;
COMMENTAIRE_MULTI :
  '/*' .*? '*/' -> skip
  ;

// ====================
// Fragments :
//   "Fragments are reusable parts of lexer rules which cannot match on their own - 
//   they need to be referenced from a lexer rule." -- ANTLR Ref. Manual.
//

// Utile pour composer les identificateurs r�serv�s insensibles � la casse
//
fragment A:('a'|'A');
fragment B:('b'|'B');
fragment C:('c'|'C');
fragment D:('d'|'D');
fragment E:('e'|'E');
fragment �:('�'|'�'); 
fragment F:('f'|'F');
fragment G:('g'|'G');
fragment H:('h'|'H');
fragment I:('i'|'I');
fragment J:('j'|'J');
fragment K:('k'|'K');
fragment L:('l'|'L');
fragment M:('m'|'M');
fragment N:('n'|'N');
fragment O:('o'|'O');
fragment P:('p'|'P');
fragment Q:('q'|'Q');
fragment R:('r'|'R');
fragment S:('s'|'S');
fragment T:('t'|'T');
fragment U:('u'|'U');
fragment V:('v'|'V');
fragment W:('w'|'W');
fragment X:('x'|'X');
fragment Y:('y'|'Y');
fragment Z:('z'|'Z');

fragment
IDENT_FIRST :
  ('a'..'z'|'A'..'Z'|'_')
  ;
fragment
IDENT_FOLLOW :
  IDENT_FIRST | ('0'..'9')
  ;
fragment
IDENT_SIMPLE :
  IDENT_FIRST IDENT_FOLLOW*
  ;
fragment
IDENT_DELIM :
  '"' (' ' | '!' | /* '"' | */ '#' |'$' | ('%' ESC_SEQ) | '&'..'~' | UNICODE_H0)* '"'
  ;
fragment
STRING_UNI :
  '\'' (' '..'$' | ('%' ESC_SEQ) | '&' | /* '\'' | */ '('..'~' | UNICODE_H0)* '\''
  ;
fragment
STRING_CAR :
  '`' (' '..'$' | ('%' ESC_SEQ) | '&'..'_' | /* '`' | */ 'a'..'~' | UNICODE_H0)* '`'
  ;
fragment
STRING_MULTI :
  (' ' | '\t')* '�' (' ' | '\t')* '\n' 
  (' '..'~' | UNICODE_H0 | '\n')*? '\n'
  (' ' | '\t')* '�' (' ' | '\t')* '\n'
  ;
fragment
UNICODE_H0 :
  '\u00A0'..'\u00FF' | '\u0100'..'\u086F' | '\u08A0'..'\uFFFF'
  ;
//  fragment
//  UNICODE_H1 : // UNICODE_H0 sauf le guillemet fermant '�'
//  '\u00A0'..'\u00BA' | '\u00BC'..'\u00FF' | '\u0100'..'\u086F' | '\u08A0'..'\uFFFF'
//  ;
fragment
ESC_SEQ :
            // Char Code Mnemonic name
      'A'   // @     %A  At-sign
    | 'B'   // BS    %B  Backspace
    | 'C'   // ^     %C  Circumflex
    | 'D'   // $     %D  Dollar
    | 'F'   // FF    %F  Form feed
    | 'H'   // \     %H  Backslash
    | 'J'   // LF    %J  Line Feed
    | 'L'   // ~     %L  Tilde
    | 'N'   //       %N  Newline (typically LF on Unix based systems and CRLF on Windows systems)
    | 'Q'   // `     %Q  Backquote (Grave accent)
    | 'R'   // CR    %R  Carriage return
    | 'S'   // #     %S  Sharp
    | 'T'   // HT    %T  Horizontal tab
    | 'U'   // NUL   %U  Null
    | 'V'   // |     %V  Vertical bar
    | 'Y'   // �     %Y  Guillemet ouvrant
    | 'Z'   // �     %Z  Guillemet fermant
    | '%'   // %     %%  Percent
    | '\''  // '     %'  Single quote
    | '"'   // "     %"  Double quote
    | '('   // [     %(  Opening bracket
    | ')'   // ]     %)  Closing bracket
    | '<'   // {     %<  Opening brace
    | '>'   // }     %>  Closing brace
    | '`'   // `     %`  Grave accent (Backquote)
    | '�'   // �     %�  Guillemet ouvrant
    | '�'   // �     %�  Guillemet fermant
    | UNICODE_ESC
    ;
fragment
UNICODE_ESC :
    '/' INTEGER '/'
    ;
fragment
EXPONENT :
    ('e'|'E') ('+'|'-')? DEC_DIGITS
    ;
fragment
HEX_DIGITS :
    HEX_DIGIT (HEX_DIGIT | '_' HEX_DIGIT)*
    ;
fragment
HEX_DIGIT :
    ('0'..'9' | 'a'..'f' | 'A'..'F')
    ;
fragment
DEC_DIGITS :
    DEC_DIGIT (DEC_DIGIT | '_' DEC_DIGIT)*
    ;
fragment
DEC_DIGIT :
    ('0'..'9')
    ;
fragment
OCT_DIGITS :
    OCT_DIGIT (OCT_DIGIT | '_' OCT_DIGIT)*
    ;
fragment
OCT_DIGIT :
    ('0'..'7')
    ;
fragment
BIN_DIGITS :
    BIN_DIGIT (BIN_DIGIT | '_' BIN_DIGIT)*
    ;
fragment
BIN_DIGIT :
    ('0'..'1')
    ;
