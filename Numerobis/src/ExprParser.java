// Generated from Expr.g4 by ANTLR 4.9.1
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ExprParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.9.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, IF=30, ELSEIF=31, ELSE=32, 
		THEN=33, FOR=34, FONCTION=35, TRANSACTION=36, RETURNS=37, DEFINIR=38, 
		INDEFINIR=39, CHANGER=40, NON=41, ORDONNE=42, TYPE=43, ENSEMBLE=44, INTERVALLE=45, 
		TUPLE=46, RELATION=47, BASE=48, CONTRAINTE=49, DANS=50, TELQUE=51, PREDICAT=52, 
		ASSERTION=53, IDENT=54, STRING=55, CHARACTER=56, INTEGER=57, FLOAT=58, 
		ESPACES=59, COMMENTAIRE_LIGNE=60, COMMENTAIRE_MULTI=61;
	public static final int
		RULE_prog = 0, RULE_instruction = 1, RULE_type_dec = 2, RULE_type_def = 3, 
		RULE_type_de_base = 4, RULE_type_primitif = 5, RULE_ordre = 6, RULE_representation = 7, 
		RULE_type_derive = 8, RULE_type_id = 9, RULE_type_construit = 10, RULE_type_ensemble = 11, 
		RULE_type_intervalle = 12, RULE_type_ordonne = 13, RULE_type_non_scalaire = 14, 
		RULE_type_tuple = 15, RULE_type_relation = 16, RULE_entete_tuple = 17, 
		RULE_attribut_dec = 18, RULE_attribut_id = 19, RULE_predicat = 20, RULE_contrainte = 21, 
		RULE_contrainte_liste = 22, RULE_contrainte_dec = 23, RULE_contrainte_id = 24, 
		RULE_expression_booleenne = 25, RULE_expression_rationnelle = 26, RULE_expression = 27, 
		RULE_calcul = 28, RULE_identifiant_calcul = 29, RULE_id_calcul_scalaire = 30, 
		RULE_id_calcul_non_scalaire = 31, RULE_fonction_appel_calcul = 32, RULE_fonction_id_calcul = 33, 
		RULE_valeur = 34, RULE_valeur_scalaire = 35, RULE_représentation_effective = 36, 
		RULE_valeur_non_scalaire = 37, RULE_valeur_tuple = 38, RULE_valeur_liste_att = 39, 
		RULE_valeur_relation = 40, RULE_valeur_liste_rel = 41, RULE_operateur = 42, 
		RULE_operateur_scalaire = 43, RULE_operateur_non_scalaire = 44, RULE_bloc = 45, 
		RULE_ifstmt = 46, RULE_param_dec = 47, RULE_param_id = 48, RULE_boucle = 49, 
		RULE_fonction_dec = 50, RULE_fonction_id = 51, RULE_fonction_appel = 52, 
		RULE_transaction_dec = 53, RULE_transaction_appel = 54, RULE_transaction_id = 55, 
		RULE_assertion_dec = 56, RULE_assertion_id = 57;
	private static String[] makeRuleNames() {
		return new String[] {
			"prog", "instruction", "type_dec", "type_def", "type_de_base", "type_primitif", 
			"ordre", "representation", "type_derive", "type_id", "type_construit", 
			"type_ensemble", "type_intervalle", "type_ordonne", "type_non_scalaire", 
			"type_tuple", "type_relation", "entete_tuple", "attribut_dec", "attribut_id", 
			"predicat", "contrainte", "contrainte_liste", "contrainte_dec", "contrainte_id", 
			"expression_booleenne", "expression_rationnelle", "expression", "calcul", 
			"identifiant_calcul", "id_calcul_scalaire", "id_calcul_non_scalaire", 
			"fonction_appel_calcul", "fonction_id_calcul", "valeur", "valeur_scalaire", 
			"représentation_effective", "valeur_non_scalaire", "valeur_tuple", "valeur_liste_att", 
			"valeur_relation", "valeur_liste_rel", "operateur", "operateur_scalaire", 
			"operateur_non_scalaire", "bloc", "ifstmt", "param_dec", "param_id", 
			"boucle", "fonction_dec", "fonction_id", "fonction_appel", "transaction_dec", 
			"transaction_appel", "transaction_id", "assertion_dec", "assertion_id"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "';'", "'fin'", "'('", "')'", "'{'", "', '", "'}'", "':'", "'<'", 
			"'>'", "'=='", "'='", "'>='", "'<='", "'!='", "'modulo'", "'||'", "'&&'", 
			"'+'", "'IN'", "'DISJOINT'", "'UNION'", "'JOIN'", "'RESTRICTION'", "'PROJECTION'", 
			"'DIFFERENCE'", "'*'", "'BEGIN'", "'END'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, "IF", "ELSEIF", "ELSE", "THEN", "FOR", 
			"FONCTION", "TRANSACTION", "RETURNS", "DEFINIR", "INDEFINIR", "CHANGER", 
			"NON", "ORDONNE", "TYPE", "ENSEMBLE", "INTERVALLE", "TUPLE", "RELATION", 
			"BASE", "CONTRAINTE", "DANS", "TELQUE", "PREDICAT", "ASSERTION", "IDENT", 
			"STRING", "CHARACTER", "INTEGER", "FLOAT", "ESPACES", "COMMENTAIRE_LIGNE", 
			"COMMENTAIRE_MULTI"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Expr.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public ExprParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ProgContext extends ParserRuleContext {
		public List<InstructionContext> instruction() {
			return getRuleContexts(InstructionContext.class);
		}
		public InstructionContext instruction(int i) {
			return getRuleContext(InstructionContext.class,i);
		}
		public ProgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prog; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterProg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitProg(this);
		}
	}

	public final ProgContext prog() throws RecognitionException {
		ProgContext _localctx = new ProgContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_prog);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(121);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__26) | (1L << DEFINIR) | (1L << NON) | (1L << TUPLE) | (1L << RELATION) | (1L << IDENT) | (1L << STRING) | (1L << INTEGER) | (1L << FLOAT))) != 0)) {
				{
				{
				setState(116);
				instruction();
				setState(117);
				match(T__0);
				}
				}
				setState(123);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(124);
			match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InstructionContext extends ParserRuleContext {
		public Type_decContext type_dec() {
			return getRuleContext(Type_decContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public OperateurContext operateur() {
			return getRuleContext(OperateurContext.class,0);
		}
		public InstructionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_instruction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterInstruction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitInstruction(this);
		}
	}

	public final InstructionContext instruction() throws RecognitionException {
		InstructionContext _localctx = new InstructionContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_instruction);
		try {
			setState(129);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(126);
				type_dec();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(127);
				expression();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(128);
				operateur();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_decContext extends ParserRuleContext {
		public TerminalNode DEFINIR() { return getToken(ExprParser.DEFINIR, 0); }
		public TerminalNode TYPE() { return getToken(ExprParser.TYPE, 0); }
		public TerminalNode IDENT() { return getToken(ExprParser.IDENT, 0); }
		public Type_defContext type_def() {
			return getRuleContext(Type_defContext.class,0);
		}
		public Type_decContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_dec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterType_dec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitType_dec(this);
		}
	}

	public final Type_decContext type_dec() throws RecognitionException {
		Type_decContext _localctx = new Type_decContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_type_dec);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(131);
			match(DEFINIR);
			setState(132);
			match(TYPE);
			setState(133);
			match(IDENT);
			setState(134);
			type_def();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_defContext extends ParserRuleContext {
		public Type_de_baseContext type_de_base() {
			return getRuleContext(Type_de_baseContext.class,0);
		}
		public Type_construitContext type_construit() {
			return getRuleContext(Type_construitContext.class,0);
		}
		public Type_non_scalaireContext type_non_scalaire() {
			return getRuleContext(Type_non_scalaireContext.class,0);
		}
		public Type_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterType_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitType_def(this);
		}
	}

	public final Type_defContext type_def() throws RecognitionException {
		Type_defContext _localctx = new Type_defContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_type_def);
		try {
			setState(139);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__2:
			case NON:
			case ORDONNE:
				enterOuterAlt(_localctx, 1);
				{
				setState(136);
				type_de_base();
				}
				break;
			case ENSEMBLE:
			case INTERVALLE:
				enterOuterAlt(_localctx, 2);
				{
				setState(137);
				type_construit();
				}
				break;
			case TUPLE:
			case RELATION:
				enterOuterAlt(_localctx, 3);
				{
				setState(138);
				type_non_scalaire();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_de_baseContext extends ParserRuleContext {
		public Type_primitifContext type_primitif() {
			return getRuleContext(Type_primitifContext.class,0);
		}
		public Type_deriveContext type_derive() {
			return getRuleContext(Type_deriveContext.class,0);
		}
		public Type_de_baseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_de_base; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterType_de_base(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitType_de_base(this);
		}
	}

	public final Type_de_baseContext type_de_base() throws RecognitionException {
		Type_de_baseContext _localctx = new Type_de_baseContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_type_de_base);
		try {
			setState(143);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NON:
			case ORDONNE:
				enterOuterAlt(_localctx, 1);
				{
				setState(141);
				type_primitif();
				}
				break;
			case T__2:
				enterOuterAlt(_localctx, 2);
				{
				setState(142);
				type_derive();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_primitifContext extends ParserRuleContext {
		public OrdreContext ordre() {
			return getRuleContext(OrdreContext.class,0);
		}
		public RepresentationContext representation() {
			return getRuleContext(RepresentationContext.class,0);
		}
		public Type_primitifContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_primitif; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterType_primitif(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitType_primitif(this);
		}
	}

	public final Type_primitifContext type_primitif() throws RecognitionException {
		Type_primitifContext _localctx = new Type_primitifContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_type_primitif);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(145);
			ordre();
			setState(146);
			representation();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrdreContext extends ParserRuleContext {
		public TerminalNode ORDONNE() { return getToken(ExprParser.ORDONNE, 0); }
		public TerminalNode NON() { return getToken(ExprParser.NON, 0); }
		public OrdreContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ordre; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterOrdre(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitOrdre(this);
		}
	}

	public final OrdreContext ordre() throws RecognitionException {
		OrdreContext _localctx = new OrdreContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_ordre);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(149);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==NON) {
				{
				setState(148);
				match(NON);
				}
			}

			setState(151);
			match(ORDONNE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RepresentationContext extends ParserRuleContext {
		public Expression_rationnelleContext expression_rationnelle() {
			return getRuleContext(Expression_rationnelleContext.class,0);
		}
		public ContrainteContext contrainte() {
			return getRuleContext(ContrainteContext.class,0);
		}
		public RepresentationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_representation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterRepresentation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitRepresentation(this);
		}
	}

	public final RepresentationContext representation() throws RecognitionException {
		RepresentationContext _localctx = new RepresentationContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_representation);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(153);
			expression_rationnelle();
			setState(155);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==CONTRAINTE) {
				{
				setState(154);
				contrainte();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_deriveContext extends ParserRuleContext {
		public Type_idContext type_id() {
			return getRuleContext(Type_idContext.class,0);
		}
		public ContrainteContext contrainte() {
			return getRuleContext(ContrainteContext.class,0);
		}
		public Type_deriveContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_derive; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterType_derive(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitType_derive(this);
		}
	}

	public final Type_deriveContext type_derive() throws RecognitionException {
		Type_deriveContext _localctx = new Type_deriveContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_type_derive);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(157);
			match(T__2);
			setState(158);
			type_id();
			setState(159);
			match(T__3);
			setState(160);
			contrainte();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_idContext extends ParserRuleContext {
		public TerminalNode IDENT() { return getToken(ExprParser.IDENT, 0); }
		public Type_idContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_id; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterType_id(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitType_id(this);
		}
	}

	public final Type_idContext type_id() throws RecognitionException {
		Type_idContext _localctx = new Type_idContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_type_id);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(162);
			match(IDENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_construitContext extends ParserRuleContext {
		public Type_ensembleContext type_ensemble() {
			return getRuleContext(Type_ensembleContext.class,0);
		}
		public Type_intervalleContext type_intervalle() {
			return getRuleContext(Type_intervalleContext.class,0);
		}
		public Type_construitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_construit; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterType_construit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitType_construit(this);
		}
	}

	public final Type_construitContext type_construit() throws RecognitionException {
		Type_construitContext _localctx = new Type_construitContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_type_construit);
		try {
			setState(166);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ENSEMBLE:
				enterOuterAlt(_localctx, 1);
				{
				setState(164);
				type_ensemble();
				}
				break;
			case INTERVALLE:
				enterOuterAlt(_localctx, 2);
				{
				setState(165);
				type_intervalle();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_ensembleContext extends ParserRuleContext {
		public TerminalNode ENSEMBLE() { return getToken(ExprParser.ENSEMBLE, 0); }
		public Type_idContext type_id() {
			return getRuleContext(Type_idContext.class,0);
		}
		public Type_ensembleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_ensemble; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterType_ensemble(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitType_ensemble(this);
		}
	}

	public final Type_ensembleContext type_ensemble() throws RecognitionException {
		Type_ensembleContext _localctx = new Type_ensembleContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_type_ensemble);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(168);
			match(ENSEMBLE);
			setState(169);
			type_id();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_intervalleContext extends ParserRuleContext {
		public TerminalNode INTERVALLE() { return getToken(ExprParser.INTERVALLE, 0); }
		public Type_ordonneContext type_ordonne() {
			return getRuleContext(Type_ordonneContext.class,0);
		}
		public Type_intervalleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_intervalle; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterType_intervalle(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitType_intervalle(this);
		}
	}

	public final Type_intervalleContext type_intervalle() throws RecognitionException {
		Type_intervalleContext _localctx = new Type_intervalleContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_type_intervalle);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(171);
			match(INTERVALLE);
			setState(172);
			type_ordonne();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_ordonneContext extends ParserRuleContext {
		public Type_idContext type_id() {
			return getRuleContext(Type_idContext.class,0);
		}
		public Type_ordonneContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_ordonne; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterType_ordonne(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitType_ordonne(this);
		}
	}

	public final Type_ordonneContext type_ordonne() throws RecognitionException {
		Type_ordonneContext _localctx = new Type_ordonneContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_type_ordonne);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(174);
			type_id();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_non_scalaireContext extends ParserRuleContext {
		public Type_tupleContext type_tuple() {
			return getRuleContext(Type_tupleContext.class,0);
		}
		public Type_relationContext type_relation() {
			return getRuleContext(Type_relationContext.class,0);
		}
		public ContrainteContext contrainte() {
			return getRuleContext(ContrainteContext.class,0);
		}
		public PredicatContext predicat() {
			return getRuleContext(PredicatContext.class,0);
		}
		public Type_non_scalaireContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_non_scalaire; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterType_non_scalaire(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitType_non_scalaire(this);
		}
	}

	public final Type_non_scalaireContext type_non_scalaire() throws RecognitionException {
		Type_non_scalaireContext _localctx = new Type_non_scalaireContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_type_non_scalaire);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(178);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TUPLE:
				{
				setState(176);
				type_tuple();
				}
				break;
			case RELATION:
				{
				setState(177);
				type_relation();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(181);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==CONTRAINTE) {
				{
				setState(180);
				contrainte();
				}
			}

			setState(184);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==PREDICAT) {
				{
				setState(183);
				predicat();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_tupleContext extends ParserRuleContext {
		public TerminalNode TUPLE() { return getToken(ExprParser.TUPLE, 0); }
		public Entete_tupleContext entete_tuple() {
			return getRuleContext(Entete_tupleContext.class,0);
		}
		public Type_tupleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_tuple; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterType_tuple(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitType_tuple(this);
		}
	}

	public final Type_tupleContext type_tuple() throws RecognitionException {
		Type_tupleContext _localctx = new Type_tupleContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_type_tuple);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(186);
			match(TUPLE);
			setState(187);
			entete_tuple();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_relationContext extends ParserRuleContext {
		public TerminalNode RELATION() { return getToken(ExprParser.RELATION, 0); }
		public Entete_tupleContext entete_tuple() {
			return getRuleContext(Entete_tupleContext.class,0);
		}
		public Type_relationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_relation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterType_relation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitType_relation(this);
		}
	}

	public final Type_relationContext type_relation() throws RecognitionException {
		Type_relationContext _localctx = new Type_relationContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_type_relation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(189);
			match(RELATION);
			setState(190);
			entete_tuple();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Entete_tupleContext extends ParserRuleContext {
		public List<Attribut_decContext> attribut_dec() {
			return getRuleContexts(Attribut_decContext.class);
		}
		public Attribut_decContext attribut_dec(int i) {
			return getRuleContext(Attribut_decContext.class,i);
		}
		public Entete_tupleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_entete_tuple; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterEntete_tuple(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitEntete_tuple(this);
		}
	}

	public final Entete_tupleContext entete_tuple() throws RecognitionException {
		Entete_tupleContext _localctx = new Entete_tupleContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_entete_tuple);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(192);
			match(T__4);
			setState(201);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IDENT) {
				{
				setState(193);
				attribut_dec();
				setState(198);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__5) {
					{
					{
					setState(194);
					match(T__5);
					setState(195);
					attribut_dec();
					}
					}
					setState(200);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(203);
			match(T__6);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Attribut_decContext extends ParserRuleContext {
		public Attribut_idContext attribut_id() {
			return getRuleContext(Attribut_idContext.class,0);
		}
		public Type_idContext type_id() {
			return getRuleContext(Type_idContext.class,0);
		}
		public Attribut_decContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_attribut_dec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterAttribut_dec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitAttribut_dec(this);
		}
	}

	public final Attribut_decContext attribut_dec() throws RecognitionException {
		Attribut_decContext _localctx = new Attribut_decContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_attribut_dec);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(205);
			attribut_id();
			setState(206);
			match(T__7);
			setState(207);
			type_id();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Attribut_idContext extends ParserRuleContext {
		public TerminalNode IDENT() { return getToken(ExprParser.IDENT, 0); }
		public Attribut_idContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_attribut_id; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterAttribut_id(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitAttribut_id(this);
		}
	}

	public final Attribut_idContext attribut_id() throws RecognitionException {
		Attribut_idContext _localctx = new Attribut_idContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_attribut_id);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(209);
			match(IDENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PredicatContext extends ParserRuleContext {
		public TerminalNode PREDICAT() { return getToken(ExprParser.PREDICAT, 0); }
		public List<TerminalNode> STRING() { return getTokens(ExprParser.STRING); }
		public TerminalNode STRING(int i) {
			return getToken(ExprParser.STRING, i);
		}
		public PredicatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_predicat; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterPredicat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitPredicat(this);
		}
	}

	public final PredicatContext predicat() throws RecognitionException {
		PredicatContext _localctx = new PredicatContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_predicat);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(211);
			match(PREDICAT);
			setState(212);
			match(T__4);
			setState(213);
			match(STRING);
			setState(217);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==STRING) {
				{
				{
				setState(214);
				match(STRING);
				}
				}
				setState(219);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(220);
			match(T__6);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ContrainteContext extends ParserRuleContext {
		public TerminalNode CONTRAINTE() { return getToken(ExprParser.CONTRAINTE, 0); }
		public Contrainte_listeContext contrainte_liste() {
			return getRuleContext(Contrainte_listeContext.class,0);
		}
		public ContrainteContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_contrainte; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterContrainte(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitContrainte(this);
		}
	}

	public final ContrainteContext contrainte() throws RecognitionException {
		ContrainteContext _localctx = new ContrainteContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_contrainte);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(222);
			match(CONTRAINTE);
			setState(223);
			contrainte_liste();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Contrainte_listeContext extends ParserRuleContext {
		public List<Contrainte_decContext> contrainte_dec() {
			return getRuleContexts(Contrainte_decContext.class);
		}
		public Contrainte_decContext contrainte_dec(int i) {
			return getRuleContext(Contrainte_decContext.class,i);
		}
		public Contrainte_listeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_contrainte_liste; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterContrainte_liste(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitContrainte_liste(this);
		}
	}

	public final Contrainte_listeContext contrainte_liste() throws RecognitionException {
		Contrainte_listeContext _localctx = new Contrainte_listeContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_contrainte_liste);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(225);
			match(T__4);
			setState(234);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__26) | (1L << NON) | (1L << TUPLE) | (1L << RELATION) | (1L << IDENT) | (1L << STRING) | (1L << INTEGER) | (1L << FLOAT))) != 0)) {
				{
				setState(226);
				contrainte_dec();
				setState(231);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__0) {
					{
					{
					setState(227);
					match(T__0);
					setState(228);
					contrainte_dec();
					}
					}
					setState(233);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(236);
			match(T__6);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Contrainte_decContext extends ParserRuleContext {
		public Expression_booleenneContext expression_booleenne() {
			return getRuleContext(Expression_booleenneContext.class,0);
		}
		public Contrainte_idContext contrainte_id() {
			return getRuleContext(Contrainte_idContext.class,0);
		}
		public Contrainte_decContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_contrainte_dec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterContrainte_dec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitContrainte_dec(this);
		}
	}

	public final Contrainte_decContext contrainte_dec() throws RecognitionException {
		Contrainte_decContext _localctx = new Contrainte_decContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_contrainte_dec);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(241);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				{
				setState(238);
				contrainte_id();
				setState(239);
				match(T__7);
				}
				break;
			}
			setState(243);
			expression_booleenne();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Contrainte_idContext extends ParserRuleContext {
		public TerminalNode IDENT() { return getToken(ExprParser.IDENT, 0); }
		public Contrainte_idContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_contrainte_id; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterContrainte_id(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitContrainte_id(this);
		}
	}

	public final Contrainte_idContext contrainte_id() throws RecognitionException {
		Contrainte_idContext _localctx = new Contrainte_idContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_contrainte_id);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(245);
			match(IDENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expression_booleenneContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Expression_booleenneContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression_booleenne; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterExpression_booleenne(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitExpression_booleenne(this);
		}
	}

	public final Expression_booleenneContext expression_booleenne() throws RecognitionException {
		Expression_booleenneContext _localctx = new Expression_booleenneContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_expression_booleenne);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(247);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expression_rationnelleContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(ExprParser.STRING, 0); }
		public Expression_rationnelleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression_rationnelle; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterExpression_rationnelle(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitExpression_rationnelle(this);
		}
	}

	public final Expression_rationnelleContext expression_rationnelle() throws RecognitionException {
		Expression_rationnelleContext _localctx = new Expression_rationnelleContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_expression_rationnelle);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(249);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ValeurContext valeur() {
			return getRuleContext(ValeurContext.class,0);
		}
		public Fonction_appel_calculContext fonction_appel_calcul() {
			return getRuleContext(Fonction_appel_calculContext.class,0);
		}
		public CalculContext calcul() {
			return getRuleContext(CalculContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitExpression(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_expression);
		try {
			setState(254);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(251);
				valeur();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(252);
				fonction_appel_calcul();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(253);
				calcul();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CalculContext extends ParserRuleContext {
		public Identifiant_calculContext identifiant_calcul() {
			return getRuleContext(Identifiant_calculContext.class,0);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public CalculContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_calcul; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterCalcul(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitCalcul(this);
		}
	}

	public final CalculContext calcul() throws RecognitionException {
		CalculContext _localctx = new CalculContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_calcul);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(256);
			identifiant_calcul();
			setState(257);
			match(T__2);
			setState(258);
			expression();
			setState(259);
			match(T__5);
			setState(260);
			expression();
			setState(261);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Identifiant_calculContext extends ParserRuleContext {
		public Id_calcul_scalaireContext id_calcul_scalaire() {
			return getRuleContext(Id_calcul_scalaireContext.class,0);
		}
		public Id_calcul_non_scalaireContext id_calcul_non_scalaire() {
			return getRuleContext(Id_calcul_non_scalaireContext.class,0);
		}
		public Identifiant_calculContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_identifiant_calcul; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterIdentifiant_calcul(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitIdentifiant_calcul(this);
		}
	}

	public final Identifiant_calculContext identifiant_calcul() throws RecognitionException {
		Identifiant_calculContext _localctx = new Identifiant_calculContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_identifiant_calcul);
		try {
			setState(265);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__8:
			case T__9:
			case T__10:
			case T__11:
			case T__12:
			case T__13:
			case T__14:
			case T__15:
			case T__16:
			case T__17:
			case T__18:
				enterOuterAlt(_localctx, 1);
				{
				setState(263);
				id_calcul_scalaire();
				}
				break;
			case T__19:
			case T__20:
			case T__21:
			case T__22:
			case T__23:
			case T__24:
			case T__25:
			case NON:
			case IDENT:
				enterOuterAlt(_localctx, 2);
				{
				setState(264);
				id_calcul_non_scalaire();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Id_calcul_scalaireContext extends ParserRuleContext {
		public Id_calcul_scalaireContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_id_calcul_scalaire; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterId_calcul_scalaire(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitId_calcul_scalaire(this);
		}
	}

	public final Id_calcul_scalaireContext id_calcul_scalaire() throws RecognitionException {
		Id_calcul_scalaireContext _localctx = new Id_calcul_scalaireContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_id_calcul_scalaire);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(267);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Id_calcul_non_scalaireContext extends ParserRuleContext {
		public TerminalNode NON() { return getToken(ExprParser.NON, 0); }
		public TerminalNode IDENT() { return getToken(ExprParser.IDENT, 0); }
		public Id_calcul_non_scalaireContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_id_calcul_non_scalaire; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterId_calcul_non_scalaire(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitId_calcul_non_scalaire(this);
		}
	}

	public final Id_calcul_non_scalaireContext id_calcul_non_scalaire() throws RecognitionException {
		Id_calcul_non_scalaireContext _localctx = new Id_calcul_non_scalaireContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_id_calcul_non_scalaire);
		int _la;
		try {
			setState(282);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__19:
			case NON:
				enterOuterAlt(_localctx, 1);
				{
				setState(270);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==NON) {
					{
					setState(269);
					match(NON);
					}
				}

				setState(272);
				match(T__19);
				}
				break;
			case T__20:
			case T__21:
				enterOuterAlt(_localctx, 2);
				{
				setState(274);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__20) {
					{
					setState(273);
					match(T__20);
					}
				}

				setState(276);
				match(T__21);
				}
				break;
			case T__22:
				enterOuterAlt(_localctx, 3);
				{
				setState(277);
				match(T__22);
				}
				break;
			case T__23:
				enterOuterAlt(_localctx, 4);
				{
				setState(278);
				match(T__23);
				}
				break;
			case T__24:
				enterOuterAlt(_localctx, 5);
				{
				setState(279);
				match(T__24);
				}
				break;
			case T__25:
				enterOuterAlt(_localctx, 6);
				{
				setState(280);
				match(T__25);
				}
				break;
			case IDENT:
				enterOuterAlt(_localctx, 7);
				{
				setState(281);
				match(IDENT);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Fonction_appel_calculContext extends ParserRuleContext {
		public Fonction_idContext fonction_id() {
			return getRuleContext(Fonction_idContext.class,0);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public Fonction_appel_calculContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fonction_appel_calcul; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterFonction_appel_calcul(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitFonction_appel_calcul(this);
		}
	}

	public final Fonction_appel_calculContext fonction_appel_calcul() throws RecognitionException {
		Fonction_appel_calculContext _localctx = new Fonction_appel_calculContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_fonction_appel_calcul);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(284);
			fonction_id();
			setState(288);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(285);
					expression();
					}
					} 
				}
				setState(290);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Fonction_id_calculContext extends ParserRuleContext {
		public TerminalNode IDENT() { return getToken(ExprParser.IDENT, 0); }
		public Fonction_id_calculContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fonction_id_calcul; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterFonction_id_calcul(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitFonction_id_calcul(this);
		}
	}

	public final Fonction_id_calculContext fonction_id_calcul() throws RecognitionException {
		Fonction_id_calculContext _localctx = new Fonction_id_calculContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_fonction_id_calcul);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(291);
			match(IDENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValeurContext extends ParserRuleContext {
		public Valeur_scalaireContext valeur_scalaire() {
			return getRuleContext(Valeur_scalaireContext.class,0);
		}
		public Valeur_non_scalaireContext valeur_non_scalaire() {
			return getRuleContext(Valeur_non_scalaireContext.class,0);
		}
		public ValeurContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valeur; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterValeur(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitValeur(this);
		}
	}

	public final ValeurContext valeur() throws RecognitionException {
		ValeurContext _localctx = new ValeurContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_valeur);
		try {
			setState(295);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IDENT:
			case STRING:
			case INTEGER:
			case FLOAT:
				enterOuterAlt(_localctx, 1);
				{
				setState(293);
				valeur_scalaire();
				}
				break;
			case T__2:
			case T__26:
			case TUPLE:
			case RELATION:
				enterOuterAlt(_localctx, 2);
				{
				setState(294);
				valeur_non_scalaire();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Valeur_scalaireContext extends ParserRuleContext {
		public Représentation_effectiveContext représentation_effective() {
			return getRuleContext(Représentation_effectiveContext.class,0);
		}
		public Valeur_scalaireContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valeur_scalaire; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterValeur_scalaire(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitValeur_scalaire(this);
		}
	}

	public final Valeur_scalaireContext valeur_scalaire() throws RecognitionException {
		Valeur_scalaireContext _localctx = new Valeur_scalaireContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_valeur_scalaire);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(297);
			représentation_effective();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Représentation_effectiveContext extends ParserRuleContext {
		public TerminalNode IDENT() { return getToken(ExprParser.IDENT, 0); }
		public TerminalNode INTEGER() { return getToken(ExprParser.INTEGER, 0); }
		public TerminalNode FLOAT() { return getToken(ExprParser.FLOAT, 0); }
		public TerminalNode STRING() { return getToken(ExprParser.STRING, 0); }
		public Représentation_effectiveContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_représentation_effective; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterReprésentation_effective(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitReprésentation_effective(this);
		}
	}

	public final Représentation_effectiveContext représentation_effective() throws RecognitionException {
		Représentation_effectiveContext _localctx = new Représentation_effectiveContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_représentation_effective);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(299);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << IDENT) | (1L << STRING) | (1L << INTEGER) | (1L << FLOAT))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Valeur_non_scalaireContext extends ParserRuleContext {
		public Valeur_tupleContext valeur_tuple() {
			return getRuleContext(Valeur_tupleContext.class,0);
		}
		public Valeur_relationContext valeur_relation() {
			return getRuleContext(Valeur_relationContext.class,0);
		}
		public Valeur_non_scalaireContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valeur_non_scalaire; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterValeur_non_scalaire(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitValeur_non_scalaire(this);
		}
	}

	public final Valeur_non_scalaireContext valeur_non_scalaire() throws RecognitionException {
		Valeur_non_scalaireContext _localctx = new Valeur_non_scalaireContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_valeur_non_scalaire);
		try {
			setState(303);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__26:
			case TUPLE:
				enterOuterAlt(_localctx, 1);
				{
				setState(301);
				valeur_tuple();
				}
				break;
			case T__2:
			case RELATION:
				enterOuterAlt(_localctx, 2);
				{
				setState(302);
				valeur_relation();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Valeur_tupleContext extends ParserRuleContext {
		public TerminalNode TUPLE() { return getToken(ExprParser.TUPLE, 0); }
		public Valeur_liste_attContext valeur_liste_att() {
			return getRuleContext(Valeur_liste_attContext.class,0);
		}
		public Valeur_tupleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valeur_tuple; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterValeur_tuple(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitValeur_tuple(this);
		}
	}

	public final Valeur_tupleContext valeur_tuple() throws RecognitionException {
		Valeur_tupleContext _localctx = new Valeur_tupleContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_valeur_tuple);
		try {
			setState(311);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TUPLE:
				enterOuterAlt(_localctx, 1);
				{
				setState(305);
				match(TUPLE);
				setState(306);
				match(T__4);
				setState(307);
				valeur_liste_att();
				setState(308);
				match(T__6);
				}
				break;
			case T__26:
				enterOuterAlt(_localctx, 2);
				{
				setState(310);
				match(T__26);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Valeur_liste_attContext extends ParserRuleContext {
		public List<Attribut_idContext> attribut_id() {
			return getRuleContexts(Attribut_idContext.class);
		}
		public Attribut_idContext attribut_id(int i) {
			return getRuleContext(Attribut_idContext.class,i);
		}
		public List<TerminalNode> TELQUE() { return getTokens(ExprParser.TELQUE); }
		public TerminalNode TELQUE(int i) {
			return getToken(ExprParser.TELQUE, i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public Valeur_liste_attContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valeur_liste_att; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterValeur_liste_att(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitValeur_liste_att(this);
		}
	}

	public final Valeur_liste_attContext valeur_liste_att() throws RecognitionException {
		Valeur_liste_attContext _localctx = new Valeur_liste_attContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_valeur_liste_att);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(329);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,28,_ctx) ) {
			case 1:
				{
				setState(313);
				attribut_id();
				setState(316);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==TELQUE) {
					{
					setState(314);
					match(TELQUE);
					setState(315);
					expression();
					}
				}

				setState(326);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(318);
						match(T__5);
						setState(319);
						attribut_id();
						setState(322);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==TELQUE) {
							{
							setState(320);
							match(TELQUE);
							setState(321);
							expression();
							}
						}

						}
						} 
					}
					setState(328);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
				}
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Valeur_relationContext extends ParserRuleContext {
		public TerminalNode RELATION() { return getToken(ExprParser.RELATION, 0); }
		public Valeur_liste_relContext valeur_liste_rel() {
			return getRuleContext(Valeur_liste_relContext.class,0);
		}
		public Valeur_relationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valeur_relation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterValeur_relation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitValeur_relation(this);
		}
	}

	public final Valeur_relationContext valeur_relation() throws RecognitionException {
		Valeur_relationContext _localctx = new Valeur_relationContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_valeur_relation);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(332);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__2) {
				{
				setState(331);
				match(T__2);
				}
			}

			setState(334);
			match(RELATION);
			setState(335);
			valeur_liste_rel();
			setState(337);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,30,_ctx) ) {
			case 1:
				{
				setState(336);
				match(T__3);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Valeur_liste_relContext extends ParserRuleContext {
		public List<Valeur_liste_attContext> valeur_liste_att() {
			return getRuleContexts(Valeur_liste_attContext.class);
		}
		public Valeur_liste_attContext valeur_liste_att(int i) {
			return getRuleContext(Valeur_liste_attContext.class,i);
		}
		public Valeur_liste_relContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valeur_liste_rel; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterValeur_liste_rel(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitValeur_liste_rel(this);
		}
	}

	public final Valeur_liste_relContext valeur_liste_rel() throws RecognitionException {
		Valeur_liste_relContext _localctx = new Valeur_liste_relContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_valeur_liste_rel);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(339);
			valeur_liste_att();
			setState(344);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,31,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(340);
					match(T__5);
					setState(341);
					valeur_liste_att();
					}
					} 
				}
				setState(346);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,31,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperateurContext extends ParserRuleContext {
		public Operateur_scalaireContext operateur_scalaire() {
			return getRuleContext(Operateur_scalaireContext.class,0);
		}
		public Operateur_non_scalaireContext operateur_non_scalaire() {
			return getRuleContext(Operateur_non_scalaireContext.class,0);
		}
		public OperateurContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operateur; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterOperateur(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitOperateur(this);
		}
	}

	public final OperateurContext operateur() throws RecognitionException {
		OperateurContext _localctx = new OperateurContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_operateur);
		try {
			setState(349);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,32,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(347);
				operateur_scalaire();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(348);
				operateur_non_scalaire();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Operateur_scalaireContext extends ParserRuleContext {
		public Fonction_decContext fonction_dec() {
			return getRuleContext(Fonction_decContext.class,0);
		}
		public Fonction_appelContext fonction_appel() {
			return getRuleContext(Fonction_appelContext.class,0);
		}
		public Operateur_scalaireContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operateur_scalaire; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterOperateur_scalaire(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitOperateur_scalaire(this);
		}
	}

	public final Operateur_scalaireContext operateur_scalaire() throws RecognitionException {
		Operateur_scalaireContext _localctx = new Operateur_scalaireContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_operateur_scalaire);
		try {
			setState(353);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DEFINIR:
				enterOuterAlt(_localctx, 1);
				{
				setState(351);
				fonction_dec();
				}
				break;
			case IDENT:
				enterOuterAlt(_localctx, 2);
				{
				setState(352);
				fonction_appel();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Operateur_non_scalaireContext extends ParserRuleContext {
		public Transaction_decContext transaction_dec() {
			return getRuleContext(Transaction_decContext.class,0);
		}
		public Transaction_appelContext transaction_appel() {
			return getRuleContext(Transaction_appelContext.class,0);
		}
		public Assertion_decContext assertion_dec() {
			return getRuleContext(Assertion_decContext.class,0);
		}
		public Operateur_non_scalaireContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operateur_non_scalaire; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterOperateur_non_scalaire(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitOperateur_non_scalaire(this);
		}
	}

	public final Operateur_non_scalaireContext operateur_non_scalaire() throws RecognitionException {
		Operateur_non_scalaireContext _localctx = new Operateur_non_scalaireContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_operateur_non_scalaire);
		try {
			setState(358);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,34,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(355);
				transaction_dec();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(356);
				transaction_appel();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(357);
				assertion_dec();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlocContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<IfstmtContext> ifstmt() {
			return getRuleContexts(IfstmtContext.class);
		}
		public IfstmtContext ifstmt(int i) {
			return getRuleContext(IfstmtContext.class,i);
		}
		public List<BoucleContext> boucle() {
			return getRuleContexts(BoucleContext.class);
		}
		public BoucleContext boucle(int i) {
			return getRuleContext(BoucleContext.class,i);
		}
		public BlocContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bloc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterBloc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitBloc(this);
		}
	}

	public final BlocContext bloc() throws RecognitionException {
		BlocContext _localctx = new BlocContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_bloc);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(360);
			match(T__27);
			setState(370);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__26) | (1L << IF) | (1L << FOR) | (1L << NON) | (1L << TUPLE) | (1L << RELATION) | (1L << IDENT) | (1L << STRING) | (1L << INTEGER) | (1L << FLOAT))) != 0)) {
				{
				{
				setState(364);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__2:
				case T__8:
				case T__9:
				case T__10:
				case T__11:
				case T__12:
				case T__13:
				case T__14:
				case T__15:
				case T__16:
				case T__17:
				case T__18:
				case T__19:
				case T__20:
				case T__21:
				case T__22:
				case T__23:
				case T__24:
				case T__25:
				case T__26:
				case NON:
				case TUPLE:
				case RELATION:
				case IDENT:
				case STRING:
				case INTEGER:
				case FLOAT:
					{
					setState(361);
					expression();
					}
					break;
				case IF:
					{
					setState(362);
					ifstmt();
					}
					break;
				case FOR:
					{
					setState(363);
					boucle();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(366);
				match(T__0);
				}
				}
				setState(372);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(373);
			match(T__28);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfstmtContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(ExprParser.IF, 0); }
		public List<Expression_booleenneContext> expression_booleenne() {
			return getRuleContexts(Expression_booleenneContext.class);
		}
		public Expression_booleenneContext expression_booleenne(int i) {
			return getRuleContext(Expression_booleenneContext.class,i);
		}
		public List<TerminalNode> THEN() { return getTokens(ExprParser.THEN); }
		public TerminalNode THEN(int i) {
			return getToken(ExprParser.THEN, i);
		}
		public List<BlocContext> bloc() {
			return getRuleContexts(BlocContext.class);
		}
		public BlocContext bloc(int i) {
			return getRuleContext(BlocContext.class,i);
		}
		public List<TerminalNode> ELSEIF() { return getTokens(ExprParser.ELSEIF); }
		public TerminalNode ELSEIF(int i) {
			return getToken(ExprParser.ELSEIF, i);
		}
		public TerminalNode ELSE() { return getToken(ExprParser.ELSE, 0); }
		public IfstmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifstmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterIfstmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitIfstmt(this);
		}
	}

	public final IfstmtContext ifstmt() throws RecognitionException {
		IfstmtContext _localctx = new IfstmtContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_ifstmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(375);
			match(IF);
			setState(376);
			match(T__2);
			setState(377);
			expression_booleenne();
			setState(378);
			match(T__3);
			setState(379);
			match(THEN);
			setState(380);
			bloc();
			setState(390);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ELSEIF) {
				{
				{
				setState(381);
				match(ELSEIF);
				setState(382);
				match(T__2);
				setState(383);
				expression_booleenne();
				setState(384);
				match(T__3);
				setState(385);
				match(THEN);
				setState(386);
				bloc();
				}
				}
				setState(392);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(396);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ELSE) {
				{
				setState(393);
				match(ELSE);
				setState(394);
				match(THEN);
				setState(395);
				bloc();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Param_decContext extends ParserRuleContext {
		public Param_idContext param_id() {
			return getRuleContext(Param_idContext.class,0);
		}
		public Type_idContext type_id() {
			return getRuleContext(Type_idContext.class,0);
		}
		public Param_decContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param_dec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterParam_dec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitParam_dec(this);
		}
	}

	public final Param_decContext param_dec() throws RecognitionException {
		Param_decContext _localctx = new Param_decContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_param_dec);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(398);
			param_id();
			setState(399);
			match(T__7);
			setState(400);
			type_id();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Param_idContext extends ParserRuleContext {
		public TerminalNode IDENT() { return getToken(ExprParser.IDENT, 0); }
		public Param_idContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_param_id; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterParam_id(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitParam_id(this);
		}
	}

	public final Param_idContext param_id() throws RecognitionException {
		Param_idContext _localctx = new Param_idContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_param_id);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(402);
			match(IDENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoucleContext extends ParserRuleContext {
		public TerminalNode FOR() { return getToken(ExprParser.FOR, 0); }
		public Expression_booleenneContext expression_booleenne() {
			return getRuleContext(Expression_booleenneContext.class,0);
		}
		public BlocContext bloc() {
			return getRuleContext(BlocContext.class,0);
		}
		public BoucleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boucle; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterBoucle(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitBoucle(this);
		}
	}

	public final BoucleContext boucle() throws RecognitionException {
		BoucleContext _localctx = new BoucleContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_boucle);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(404);
			match(FOR);
			setState(405);
			match(T__2);
			setState(406);
			expression_booleenne();
			setState(407);
			match(T__3);
			setState(408);
			bloc();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Fonction_decContext extends ParserRuleContext {
		public TerminalNode DEFINIR() { return getToken(ExprParser.DEFINIR, 0); }
		public TerminalNode FONCTION() { return getToken(ExprParser.FONCTION, 0); }
		public Fonction_idContext fonction_id() {
			return getRuleContext(Fonction_idContext.class,0);
		}
		public TerminalNode RETURNS() { return getToken(ExprParser.RETURNS, 0); }
		public Type_idContext type_id() {
			return getRuleContext(Type_idContext.class,0);
		}
		public BlocContext bloc() {
			return getRuleContext(BlocContext.class,0);
		}
		public List<Param_decContext> param_dec() {
			return getRuleContexts(Param_decContext.class);
		}
		public Param_decContext param_dec(int i) {
			return getRuleContext(Param_decContext.class,i);
		}
		public Fonction_decContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fonction_dec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterFonction_dec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitFonction_dec(this);
		}
	}

	public final Fonction_decContext fonction_dec() throws RecognitionException {
		Fonction_decContext _localctx = new Fonction_decContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_fonction_dec);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(410);
			match(DEFINIR);
			setState(411);
			match(FONCTION);
			setState(412);
			fonction_id();
			setState(413);
			match(T__2);
			setState(422);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IDENT) {
				{
				setState(414);
				param_dec();
				setState(419);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__5) {
					{
					{
					setState(415);
					match(T__5);
					setState(416);
					param_dec();
					}
					}
					setState(421);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(424);
			match(T__3);
			setState(425);
			match(RETURNS);
			setState(426);
			type_id();
			setState(427);
			bloc();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Fonction_idContext extends ParserRuleContext {
		public TerminalNode IDENT() { return getToken(ExprParser.IDENT, 0); }
		public Fonction_idContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fonction_id; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterFonction_id(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitFonction_id(this);
		}
	}

	public final Fonction_idContext fonction_id() throws RecognitionException {
		Fonction_idContext _localctx = new Fonction_idContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_fonction_id);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(429);
			match(IDENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Fonction_appelContext extends ParserRuleContext {
		public Fonction_idContext fonction_id() {
			return getRuleContext(Fonction_idContext.class,0);
		}
		public List<Représentation_effectiveContext> représentation_effective() {
			return getRuleContexts(Représentation_effectiveContext.class);
		}
		public Représentation_effectiveContext représentation_effective(int i) {
			return getRuleContext(Représentation_effectiveContext.class,i);
		}
		public Fonction_appelContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fonction_appel; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterFonction_appel(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitFonction_appel(this);
		}
	}

	public final Fonction_appelContext fonction_appel() throws RecognitionException {
		Fonction_appelContext _localctx = new Fonction_appelContext(_ctx, getState());
		enterRule(_localctx, 104, RULE_fonction_appel);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(431);
			fonction_id();
			setState(432);
			match(T__2);
			setState(441);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << IDENT) | (1L << STRING) | (1L << INTEGER) | (1L << FLOAT))) != 0)) {
				{
				setState(433);
				représentation_effective();
				setState(438);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__5) {
					{
					{
					setState(434);
					match(T__5);
					setState(435);
					représentation_effective();
					}
					}
					setState(440);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(443);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Transaction_decContext extends ParserRuleContext {
		public TerminalNode DEFINIR() { return getToken(ExprParser.DEFINIR, 0); }
		public TerminalNode TRANSACTION() { return getToken(ExprParser.TRANSACTION, 0); }
		public Transaction_idContext transaction_id() {
			return getRuleContext(Transaction_idContext.class,0);
		}
		public BlocContext bloc() {
			return getRuleContext(BlocContext.class,0);
		}
		public List<Param_decContext> param_dec() {
			return getRuleContexts(Param_decContext.class);
		}
		public Param_decContext param_dec(int i) {
			return getRuleContext(Param_decContext.class,i);
		}
		public Transaction_decContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_transaction_dec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterTransaction_dec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitTransaction_dec(this);
		}
	}

	public final Transaction_decContext transaction_dec() throws RecognitionException {
		Transaction_decContext _localctx = new Transaction_decContext(_ctx, getState());
		enterRule(_localctx, 106, RULE_transaction_dec);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(445);
			match(DEFINIR);
			setState(446);
			match(TRANSACTION);
			setState(447);
			transaction_id();
			setState(448);
			match(T__2);
			setState(457);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IDENT) {
				{
				setState(449);
				param_dec();
				setState(454);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__5) {
					{
					{
					setState(450);
					match(T__5);
					setState(451);
					param_dec();
					}
					}
					setState(456);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(459);
			match(T__3);
			setState(460);
			bloc();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Transaction_appelContext extends ParserRuleContext {
		public Transaction_idContext transaction_id() {
			return getRuleContext(Transaction_idContext.class,0);
		}
		public List<Représentation_effectiveContext> représentation_effective() {
			return getRuleContexts(Représentation_effectiveContext.class);
		}
		public Représentation_effectiveContext représentation_effective(int i) {
			return getRuleContext(Représentation_effectiveContext.class,i);
		}
		public Transaction_appelContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_transaction_appel; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterTransaction_appel(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitTransaction_appel(this);
		}
	}

	public final Transaction_appelContext transaction_appel() throws RecognitionException {
		Transaction_appelContext _localctx = new Transaction_appelContext(_ctx, getState());
		enterRule(_localctx, 108, RULE_transaction_appel);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(462);
			transaction_id();
			setState(463);
			match(T__2);
			setState(472);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << IDENT) | (1L << STRING) | (1L << INTEGER) | (1L << FLOAT))) != 0)) {
				{
				setState(464);
				représentation_effective();
				setState(469);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__5) {
					{
					{
					setState(465);
					match(T__5);
					setState(466);
					représentation_effective();
					}
					}
					setState(471);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(474);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Transaction_idContext extends ParserRuleContext {
		public TerminalNode IDENT() { return getToken(ExprParser.IDENT, 0); }
		public Transaction_idContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_transaction_id; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterTransaction_id(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitTransaction_id(this);
		}
	}

	public final Transaction_idContext transaction_id() throws RecognitionException {
		Transaction_idContext _localctx = new Transaction_idContext(_ctx, getState());
		enterRule(_localctx, 110, RULE_transaction_id);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(476);
			match(IDENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assertion_decContext extends ParserRuleContext {
		public TerminalNode DEFINIR() { return getToken(ExprParser.DEFINIR, 0); }
		public TerminalNode ASSERTION() { return getToken(ExprParser.ASSERTION, 0); }
		public Assertion_idContext assertion_id() {
			return getRuleContext(Assertion_idContext.class,0);
		}
		public BlocContext bloc() {
			return getRuleContext(BlocContext.class,0);
		}
		public PredicatContext predicat() {
			return getRuleContext(PredicatContext.class,0);
		}
		public Assertion_decContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assertion_dec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterAssertion_dec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitAssertion_dec(this);
		}
	}

	public final Assertion_decContext assertion_dec() throws RecognitionException {
		Assertion_decContext _localctx = new Assertion_decContext(_ctx, getState());
		enterRule(_localctx, 112, RULE_assertion_dec);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(478);
			match(DEFINIR);
			setState(479);
			match(ASSERTION);
			setState(480);
			assertion_id();
			setState(482);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==PREDICAT) {
				{
				setState(481);
				predicat();
				}
			}

			setState(484);
			bloc();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assertion_idContext extends ParserRuleContext {
		public TerminalNode IDENT() { return getToken(ExprParser.IDENT, 0); }
		public Assertion_idContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assertion_id; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).enterAssertion_id(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ExprListener ) ((ExprListener)listener).exitAssertion_id(this);
		}
	}

	public final Assertion_idContext assertion_id() throws RecognitionException {
		Assertion_idContext _localctx = new Assertion_idContext(_ctx, getState());
		enterRule(_localctx, 114, RULE_assertion_id);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(486);
			match(IDENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3?\u01eb\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\3\2\3\2\3\2"+
		"\7\2z\n\2\f\2\16\2}\13\2\3\2\3\2\3\3\3\3\3\3\5\3\u0084\n\3\3\4\3\4\3\4"+
		"\3\4\3\4\3\5\3\5\3\5\5\5\u008e\n\5\3\6\3\6\5\6\u0092\n\6\3\7\3\7\3\7\3"+
		"\b\5\b\u0098\n\b\3\b\3\b\3\t\3\t\5\t\u009e\n\t\3\n\3\n\3\n\3\n\3\n\3\13"+
		"\3\13\3\f\3\f\5\f\u00a9\n\f\3\r\3\r\3\r\3\16\3\16\3\16\3\17\3\17\3\20"+
		"\3\20\5\20\u00b5\n\20\3\20\5\20\u00b8\n\20\3\20\5\20\u00bb\n\20\3\21\3"+
		"\21\3\21\3\22\3\22\3\22\3\23\3\23\3\23\3\23\7\23\u00c7\n\23\f\23\16\23"+
		"\u00ca\13\23\5\23\u00cc\n\23\3\23\3\23\3\24\3\24\3\24\3\24\3\25\3\25\3"+
		"\26\3\26\3\26\3\26\7\26\u00da\n\26\f\26\16\26\u00dd\13\26\3\26\3\26\3"+
		"\27\3\27\3\27\3\30\3\30\3\30\3\30\7\30\u00e8\n\30\f\30\16\30\u00eb\13"+
		"\30\5\30\u00ed\n\30\3\30\3\30\3\31\3\31\3\31\5\31\u00f4\n\31\3\31\3\31"+
		"\3\32\3\32\3\33\3\33\3\34\3\34\3\35\3\35\3\35\5\35\u0101\n\35\3\36\3\36"+
		"\3\36\3\36\3\36\3\36\3\36\3\37\3\37\5\37\u010c\n\37\3 \3 \3!\5!\u0111"+
		"\n!\3!\3!\5!\u0115\n!\3!\3!\3!\3!\3!\3!\5!\u011d\n!\3\"\3\"\7\"\u0121"+
		"\n\"\f\"\16\"\u0124\13\"\3#\3#\3$\3$\5$\u012a\n$\3%\3%\3&\3&\3\'\3\'\5"+
		"\'\u0132\n\'\3(\3(\3(\3(\3(\3(\5(\u013a\n(\3)\3)\3)\5)\u013f\n)\3)\3)"+
		"\3)\3)\5)\u0145\n)\7)\u0147\n)\f)\16)\u014a\13)\5)\u014c\n)\3*\5*\u014f"+
		"\n*\3*\3*\3*\5*\u0154\n*\3+\3+\3+\7+\u0159\n+\f+\16+\u015c\13+\3,\3,\5"+
		",\u0160\n,\3-\3-\5-\u0164\n-\3.\3.\3.\5.\u0169\n.\3/\3/\3/\3/\5/\u016f"+
		"\n/\3/\3/\7/\u0173\n/\f/\16/\u0176\13/\3/\3/\3\60\3\60\3\60\3\60\3\60"+
		"\3\60\3\60\3\60\3\60\3\60\3\60\3\60\3\60\7\60\u0187\n\60\f\60\16\60\u018a"+
		"\13\60\3\60\3\60\3\60\5\60\u018f\n\60\3\61\3\61\3\61\3\61\3\62\3\62\3"+
		"\63\3\63\3\63\3\63\3\63\3\63\3\64\3\64\3\64\3\64\3\64\3\64\3\64\7\64\u01a4"+
		"\n\64\f\64\16\64\u01a7\13\64\5\64\u01a9\n\64\3\64\3\64\3\64\3\64\3\64"+
		"\3\65\3\65\3\66\3\66\3\66\3\66\3\66\7\66\u01b7\n\66\f\66\16\66\u01ba\13"+
		"\66\5\66\u01bc\n\66\3\66\3\66\3\67\3\67\3\67\3\67\3\67\3\67\3\67\7\67"+
		"\u01c7\n\67\f\67\16\67\u01ca\13\67\5\67\u01cc\n\67\3\67\3\67\3\67\38\3"+
		"8\38\38\38\78\u01d6\n8\f8\168\u01d9\138\58\u01db\n8\38\38\39\39\3:\3:"+
		"\3:\3:\5:\u01e5\n:\3:\3:\3;\3;\3;\2\2<\2\4\6\b\n\f\16\20\22\24\26\30\32"+
		"\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLNPRTVXZ\\^`bdfhjlnprt\2\4\3\2\13"+
		"\25\4\289;<\2\u01ea\2{\3\2\2\2\4\u0083\3\2\2\2\6\u0085\3\2\2\2\b\u008d"+
		"\3\2\2\2\n\u0091\3\2\2\2\f\u0093\3\2\2\2\16\u0097\3\2\2\2\20\u009b\3\2"+
		"\2\2\22\u009f\3\2\2\2\24\u00a4\3\2\2\2\26\u00a8\3\2\2\2\30\u00aa\3\2\2"+
		"\2\32\u00ad\3\2\2\2\34\u00b0\3\2\2\2\36\u00b4\3\2\2\2 \u00bc\3\2\2\2\""+
		"\u00bf\3\2\2\2$\u00c2\3\2\2\2&\u00cf\3\2\2\2(\u00d3\3\2\2\2*\u00d5\3\2"+
		"\2\2,\u00e0\3\2\2\2.\u00e3\3\2\2\2\60\u00f3\3\2\2\2\62\u00f7\3\2\2\2\64"+
		"\u00f9\3\2\2\2\66\u00fb\3\2\2\28\u0100\3\2\2\2:\u0102\3\2\2\2<\u010b\3"+
		"\2\2\2>\u010d\3\2\2\2@\u011c\3\2\2\2B\u011e\3\2\2\2D\u0125\3\2\2\2F\u0129"+
		"\3\2\2\2H\u012b\3\2\2\2J\u012d\3\2\2\2L\u0131\3\2\2\2N\u0139\3\2\2\2P"+
		"\u014b\3\2\2\2R\u014e\3\2\2\2T\u0155\3\2\2\2V\u015f\3\2\2\2X\u0163\3\2"+
		"\2\2Z\u0168\3\2\2\2\\\u016a\3\2\2\2^\u0179\3\2\2\2`\u0190\3\2\2\2b\u0194"+
		"\3\2\2\2d\u0196\3\2\2\2f\u019c\3\2\2\2h\u01af\3\2\2\2j\u01b1\3\2\2\2l"+
		"\u01bf\3\2\2\2n\u01d0\3\2\2\2p\u01de\3\2\2\2r\u01e0\3\2\2\2t\u01e8\3\2"+
		"\2\2vw\5\4\3\2wx\7\3\2\2xz\3\2\2\2yv\3\2\2\2z}\3\2\2\2{y\3\2\2\2{|\3\2"+
		"\2\2|~\3\2\2\2}{\3\2\2\2~\177\7\4\2\2\177\3\3\2\2\2\u0080\u0084\5\6\4"+
		"\2\u0081\u0084\58\35\2\u0082\u0084\5V,\2\u0083\u0080\3\2\2\2\u0083\u0081"+
		"\3\2\2\2\u0083\u0082\3\2\2\2\u0084\5\3\2\2\2\u0085\u0086\7(\2\2\u0086"+
		"\u0087\7-\2\2\u0087\u0088\78\2\2\u0088\u0089\5\b\5\2\u0089\7\3\2\2\2\u008a"+
		"\u008e\5\n\6\2\u008b\u008e\5\26\f\2\u008c\u008e\5\36\20\2\u008d\u008a"+
		"\3\2\2\2\u008d\u008b\3\2\2\2\u008d\u008c\3\2\2\2\u008e\t\3\2\2\2\u008f"+
		"\u0092\5\f\7\2\u0090\u0092\5\22\n\2\u0091\u008f\3\2\2\2\u0091\u0090\3"+
		"\2\2\2\u0092\13\3\2\2\2\u0093\u0094\5\16\b\2\u0094\u0095\5\20\t\2\u0095"+
		"\r\3\2\2\2\u0096\u0098\7+\2\2\u0097\u0096\3\2\2\2\u0097\u0098\3\2\2\2"+
		"\u0098\u0099\3\2\2\2\u0099\u009a\7,\2\2\u009a\17\3\2\2\2\u009b\u009d\5"+
		"\66\34\2\u009c\u009e\5,\27\2\u009d\u009c\3\2\2\2\u009d\u009e\3\2\2\2\u009e"+
		"\21\3\2\2\2\u009f\u00a0\7\5\2\2\u00a0\u00a1\5\24\13\2\u00a1\u00a2\7\6"+
		"\2\2\u00a2\u00a3\5,\27\2\u00a3\23\3\2\2\2\u00a4\u00a5\78\2\2\u00a5\25"+
		"\3\2\2\2\u00a6\u00a9\5\30\r\2\u00a7\u00a9\5\32\16\2\u00a8\u00a6\3\2\2"+
		"\2\u00a8\u00a7\3\2\2\2\u00a9\27\3\2\2\2\u00aa\u00ab\7.\2\2\u00ab\u00ac"+
		"\5\24\13\2\u00ac\31\3\2\2\2\u00ad\u00ae\7/\2\2\u00ae\u00af\5\34\17\2\u00af"+
		"\33\3\2\2\2\u00b0\u00b1\5\24\13\2\u00b1\35\3\2\2\2\u00b2\u00b5\5 \21\2"+
		"\u00b3\u00b5\5\"\22\2\u00b4\u00b2\3\2\2\2\u00b4\u00b3\3\2\2\2\u00b5\u00b7"+
		"\3\2\2\2\u00b6\u00b8\5,\27\2\u00b7\u00b6\3\2\2\2\u00b7\u00b8\3\2\2\2\u00b8"+
		"\u00ba\3\2\2\2\u00b9\u00bb\5*\26\2\u00ba\u00b9\3\2\2\2\u00ba\u00bb\3\2"+
		"\2\2\u00bb\37\3\2\2\2\u00bc\u00bd\7\60\2\2\u00bd\u00be\5$\23\2\u00be!"+
		"\3\2\2\2\u00bf\u00c0\7\61\2\2\u00c0\u00c1\5$\23\2\u00c1#\3\2\2\2\u00c2"+
		"\u00cb\7\7\2\2\u00c3\u00c8\5&\24\2\u00c4\u00c5\7\b\2\2\u00c5\u00c7\5&"+
		"\24\2\u00c6\u00c4\3\2\2\2\u00c7\u00ca\3\2\2\2\u00c8\u00c6\3\2\2\2\u00c8"+
		"\u00c9\3\2\2\2\u00c9\u00cc\3\2\2\2\u00ca\u00c8\3\2\2\2\u00cb\u00c3\3\2"+
		"\2\2\u00cb\u00cc\3\2\2\2\u00cc\u00cd\3\2\2\2\u00cd\u00ce\7\t\2\2\u00ce"+
		"%\3\2\2\2\u00cf\u00d0\5(\25\2\u00d0\u00d1\7\n\2\2\u00d1\u00d2\5\24\13"+
		"\2\u00d2\'\3\2\2\2\u00d3\u00d4\78\2\2\u00d4)\3\2\2\2\u00d5\u00d6\7\66"+
		"\2\2\u00d6\u00d7\7\7\2\2\u00d7\u00db\79\2\2\u00d8\u00da\79\2\2\u00d9\u00d8"+
		"\3\2\2\2\u00da\u00dd\3\2\2\2\u00db\u00d9\3\2\2\2\u00db\u00dc\3\2\2\2\u00dc"+
		"\u00de\3\2\2\2\u00dd\u00db\3\2\2\2\u00de\u00df\7\t\2\2\u00df+\3\2\2\2"+
		"\u00e0\u00e1\7\63\2\2\u00e1\u00e2\5.\30\2\u00e2-\3\2\2\2\u00e3\u00ec\7"+
		"\7\2\2\u00e4\u00e9\5\60\31\2\u00e5\u00e6\7\3\2\2\u00e6\u00e8\5\60\31\2"+
		"\u00e7\u00e5\3\2\2\2\u00e8\u00eb\3\2\2\2\u00e9\u00e7\3\2\2\2\u00e9\u00ea"+
		"\3\2\2\2\u00ea\u00ed\3\2\2\2\u00eb\u00e9\3\2\2\2\u00ec\u00e4\3\2\2\2\u00ec"+
		"\u00ed\3\2\2\2\u00ed\u00ee\3\2\2\2\u00ee\u00ef\7\t\2\2\u00ef/\3\2\2\2"+
		"\u00f0\u00f1\5\62\32\2\u00f1\u00f2\7\n\2\2\u00f2\u00f4\3\2\2\2\u00f3\u00f0"+
		"\3\2\2\2\u00f3\u00f4\3\2\2\2\u00f4\u00f5\3\2\2\2\u00f5\u00f6\5\64\33\2"+
		"\u00f6\61\3\2\2\2\u00f7\u00f8\78\2\2\u00f8\63\3\2\2\2\u00f9\u00fa\58\35"+
		"\2\u00fa\65\3\2\2\2\u00fb\u00fc\79\2\2\u00fc\67\3\2\2\2\u00fd\u0101\5"+
		"F$\2\u00fe\u0101\5B\"\2\u00ff\u0101\5:\36\2\u0100\u00fd\3\2\2\2\u0100"+
		"\u00fe\3\2\2\2\u0100\u00ff\3\2\2\2\u01019\3\2\2\2\u0102\u0103\5<\37\2"+
		"\u0103\u0104\7\5\2\2\u0104\u0105\58\35\2\u0105\u0106\7\b\2\2\u0106\u0107"+
		"\58\35\2\u0107\u0108\7\6\2\2\u0108;\3\2\2\2\u0109\u010c\5> \2\u010a\u010c"+
		"\5@!\2\u010b\u0109\3\2\2\2\u010b\u010a\3\2\2\2\u010c=\3\2\2\2\u010d\u010e"+
		"\t\2\2\2\u010e?\3\2\2\2\u010f\u0111\7+\2\2\u0110\u010f\3\2\2\2\u0110\u0111"+
		"\3\2\2\2\u0111\u0112\3\2\2\2\u0112\u011d\7\26\2\2\u0113\u0115\7\27\2\2"+
		"\u0114\u0113\3\2\2\2\u0114\u0115\3\2\2\2\u0115\u0116\3\2\2\2\u0116\u011d"+
		"\7\30\2\2\u0117\u011d\7\31\2\2\u0118\u011d\7\32\2\2\u0119\u011d\7\33\2"+
		"\2\u011a\u011d\7\34\2\2\u011b\u011d\78\2\2\u011c\u0110\3\2\2\2\u011c\u0114"+
		"\3\2\2\2\u011c\u0117\3\2\2\2\u011c\u0118\3\2\2\2\u011c\u0119\3\2\2\2\u011c"+
		"\u011a\3\2\2\2\u011c\u011b\3\2\2\2\u011dA\3\2\2\2\u011e\u0122\5h\65\2"+
		"\u011f\u0121\58\35\2\u0120\u011f\3\2\2\2\u0121\u0124\3\2\2\2\u0122\u0120"+
		"\3\2\2\2\u0122\u0123\3\2\2\2\u0123C\3\2\2\2\u0124\u0122\3\2\2\2\u0125"+
		"\u0126\78\2\2\u0126E\3\2\2\2\u0127\u012a\5H%\2\u0128\u012a\5L\'\2\u0129"+
		"\u0127\3\2\2\2\u0129\u0128\3\2\2\2\u012aG\3\2\2\2\u012b\u012c\5J&\2\u012c"+
		"I\3\2\2\2\u012d\u012e\t\3\2\2\u012eK\3\2\2\2\u012f\u0132\5N(\2\u0130\u0132"+
		"\5R*\2\u0131\u012f\3\2\2\2\u0131\u0130\3\2\2\2\u0132M\3\2\2\2\u0133\u0134"+
		"\7\60\2\2\u0134\u0135\7\7\2\2\u0135\u0136\5P)\2\u0136\u0137\7\t\2\2\u0137"+
		"\u013a\3\2\2\2\u0138\u013a\7\35\2\2\u0139\u0133\3\2\2\2\u0139\u0138\3"+
		"\2\2\2\u013aO\3\2\2\2\u013b\u013e\5(\25\2\u013c\u013d\7\65\2\2\u013d\u013f"+
		"\58\35\2\u013e\u013c\3\2\2\2\u013e\u013f\3\2\2\2\u013f\u0148\3\2\2\2\u0140"+
		"\u0141\7\b\2\2\u0141\u0144\5(\25\2\u0142\u0143\7\65\2\2\u0143\u0145\5"+
		"8\35\2\u0144\u0142\3\2\2\2\u0144\u0145\3\2\2\2\u0145\u0147\3\2\2\2\u0146"+
		"\u0140\3\2\2\2\u0147\u014a\3\2\2\2\u0148\u0146\3\2\2\2\u0148\u0149\3\2"+
		"\2\2\u0149\u014c\3\2\2\2\u014a\u0148\3\2\2\2\u014b\u013b\3\2\2\2\u014b"+
		"\u014c\3\2\2\2\u014cQ\3\2\2\2\u014d\u014f\7\5\2\2\u014e\u014d\3\2\2\2"+
		"\u014e\u014f\3\2\2\2\u014f\u0150\3\2\2\2\u0150\u0151\7\61\2\2\u0151\u0153"+
		"\5T+\2\u0152\u0154\7\6\2\2\u0153\u0152\3\2\2\2\u0153\u0154\3\2\2\2\u0154"+
		"S\3\2\2\2\u0155\u015a\5P)\2\u0156\u0157\7\b\2\2\u0157\u0159\5P)\2\u0158"+
		"\u0156\3\2\2\2\u0159\u015c\3\2\2\2\u015a\u0158\3\2\2\2\u015a\u015b\3\2"+
		"\2\2\u015bU\3\2\2\2\u015c\u015a\3\2\2\2\u015d\u0160\5X-\2\u015e\u0160"+
		"\5Z.\2\u015f\u015d\3\2\2\2\u015f\u015e\3\2\2\2\u0160W\3\2\2\2\u0161\u0164"+
		"\5f\64\2\u0162\u0164\5j\66\2\u0163\u0161\3\2\2\2\u0163\u0162\3\2\2\2\u0164"+
		"Y\3\2\2\2\u0165\u0169\5l\67\2\u0166\u0169\5n8\2\u0167\u0169\5r:\2\u0168"+
		"\u0165\3\2\2\2\u0168\u0166\3\2\2\2\u0168\u0167\3\2\2\2\u0169[\3\2\2\2"+
		"\u016a\u0174\7\36\2\2\u016b\u016f\58\35\2\u016c\u016f\5^\60\2\u016d\u016f"+
		"\5d\63\2\u016e\u016b\3\2\2\2\u016e\u016c\3\2\2\2\u016e\u016d\3\2\2\2\u016f"+
		"\u0170\3\2\2\2\u0170\u0171\7\3\2\2\u0171\u0173\3\2\2\2\u0172\u016e\3\2"+
		"\2\2\u0173\u0176\3\2\2\2\u0174\u0172\3\2\2\2\u0174\u0175\3\2\2\2\u0175"+
		"\u0177\3\2\2\2\u0176\u0174\3\2\2\2\u0177\u0178\7\37\2\2\u0178]\3\2\2\2"+
		"\u0179\u017a\7 \2\2\u017a\u017b\7\5\2\2\u017b\u017c\5\64\33\2\u017c\u017d"+
		"\7\6\2\2\u017d\u017e\7#\2\2\u017e\u0188\5\\/\2\u017f\u0180\7!\2\2\u0180"+
		"\u0181\7\5\2\2\u0181\u0182\5\64\33\2\u0182\u0183\7\6\2\2\u0183\u0184\7"+
		"#\2\2\u0184\u0185\5\\/\2\u0185\u0187\3\2\2\2\u0186\u017f\3\2\2\2\u0187"+
		"\u018a\3\2\2\2\u0188\u0186\3\2\2\2\u0188\u0189\3\2\2\2\u0189\u018e\3\2"+
		"\2\2\u018a\u0188\3\2\2\2\u018b\u018c\7\"\2\2\u018c\u018d\7#\2\2\u018d"+
		"\u018f\5\\/\2\u018e\u018b\3\2\2\2\u018e\u018f\3\2\2\2\u018f_\3\2\2\2\u0190"+
		"\u0191\5b\62\2\u0191\u0192\7\n\2\2\u0192\u0193\5\24\13\2\u0193a\3\2\2"+
		"\2\u0194\u0195\78\2\2\u0195c\3\2\2\2\u0196\u0197\7$\2\2\u0197\u0198\7"+
		"\5\2\2\u0198\u0199\5\64\33\2\u0199\u019a\7\6\2\2\u019a\u019b\5\\/\2\u019b"+
		"e\3\2\2\2\u019c\u019d\7(\2\2\u019d\u019e\7%\2\2\u019e\u019f\5h\65\2\u019f"+
		"\u01a8\7\5\2\2\u01a0\u01a5\5`\61\2\u01a1\u01a2\7\b\2\2\u01a2\u01a4\5`"+
		"\61\2\u01a3\u01a1\3\2\2\2\u01a4\u01a7\3\2\2\2\u01a5\u01a3\3\2\2\2\u01a5"+
		"\u01a6\3\2\2\2\u01a6\u01a9\3\2\2\2\u01a7\u01a5\3\2\2\2\u01a8\u01a0\3\2"+
		"\2\2\u01a8\u01a9\3\2\2\2\u01a9\u01aa\3\2\2\2\u01aa\u01ab\7\6\2\2\u01ab"+
		"\u01ac\7\'\2\2\u01ac\u01ad\5\24\13\2\u01ad\u01ae\5\\/\2\u01aeg\3\2\2\2"+
		"\u01af\u01b0\78\2\2\u01b0i\3\2\2\2\u01b1\u01b2\5h\65\2\u01b2\u01bb\7\5"+
		"\2\2\u01b3\u01b8\5J&\2\u01b4\u01b5\7\b\2\2\u01b5\u01b7\5J&\2\u01b6\u01b4"+
		"\3\2\2\2\u01b7\u01ba\3\2\2\2\u01b8\u01b6\3\2\2\2\u01b8\u01b9\3\2\2\2\u01b9"+
		"\u01bc\3\2\2\2\u01ba\u01b8\3\2\2\2\u01bb\u01b3\3\2\2\2\u01bb\u01bc\3\2"+
		"\2\2\u01bc\u01bd\3\2\2\2\u01bd\u01be\7\6\2\2\u01bek\3\2\2\2\u01bf\u01c0"+
		"\7(\2\2\u01c0\u01c1\7&\2\2\u01c1\u01c2\5p9\2\u01c2\u01cb\7\5\2\2\u01c3"+
		"\u01c8\5`\61\2\u01c4\u01c5\7\b\2\2\u01c5\u01c7\5`\61\2\u01c6\u01c4\3\2"+
		"\2\2\u01c7\u01ca\3\2\2\2\u01c8\u01c6\3\2\2\2\u01c8\u01c9\3\2\2\2\u01c9"+
		"\u01cc\3\2\2\2\u01ca\u01c8\3\2\2\2\u01cb\u01c3\3\2\2\2\u01cb\u01cc\3\2"+
		"\2\2\u01cc\u01cd\3\2\2\2\u01cd\u01ce\7\6\2\2\u01ce\u01cf\5\\/\2\u01cf"+
		"m\3\2\2\2\u01d0\u01d1\5p9\2\u01d1\u01da\7\5\2\2\u01d2\u01d7\5J&\2\u01d3"+
		"\u01d4\7\b\2\2\u01d4\u01d6\5J&\2\u01d5\u01d3\3\2\2\2\u01d6\u01d9\3\2\2"+
		"\2\u01d7\u01d5\3\2\2\2\u01d7\u01d8\3\2\2\2\u01d8\u01db\3\2\2\2\u01d9\u01d7"+
		"\3\2\2\2\u01da\u01d2\3\2\2\2\u01da\u01db\3\2\2\2\u01db\u01dc\3\2\2\2\u01dc"+
		"\u01dd\7\6\2\2\u01ddo\3\2\2\2\u01de\u01df\78\2\2\u01dfq\3\2\2\2\u01e0"+
		"\u01e1\7(\2\2\u01e1\u01e2\7\67\2\2\u01e2\u01e4\5t;\2\u01e3\u01e5\5*\26"+
		"\2\u01e4\u01e3\3\2\2\2\u01e4\u01e5\3\2\2\2\u01e5\u01e6\3\2\2\2\u01e6\u01e7"+
		"\5\\/\2\u01e7s\3\2\2\2\u01e8\u01e9\78\2\2\u01e9u\3\2\2\2\62{\u0083\u008d"+
		"\u0091\u0097\u009d\u00a8\u00b4\u00b7\u00ba\u00c8\u00cb\u00db\u00e9\u00ec"+
		"\u00f3\u0100\u010b\u0110\u0114\u011c\u0122\u0129\u0131\u0139\u013e\u0144"+
		"\u0148\u014b\u014e\u0153\u015a\u015f\u0163\u0168\u016e\u0174\u0188\u018e"+
		"\u01a5\u01a8\u01b8\u01bb\u01c8\u01cb\u01d7\u01da\u01e4";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}