

import java.io.File;
import java.io.IOException;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;



public class Analyseur {

	/**
	 * Constructeur
	 */
	public Analyseur() {
	}

	/**
	 * Analyser un fichier qui correspond à la grammaire
	 */
	public void analyser(File input) {
		try {
			CharStream stream = CharStreams.fromFileName(input.getAbsolutePath());
			// Intialisation
			ExprLexer lexer = new ExprLexer(stream);
			CommonTokenStream symboles = new CommonTokenStream(lexer);
			ExprParser parser = new ExprParser(symboles);
			// Configurer la récupération des erreurs
			parser.addErrorListener(new ErreurSyntaxe());
			// Débuter l'analyse à partir d'une expression
			parser.prog();
		} catch (IOException e) {
			System.err.println("Fichier introuvable : " + input.getAbsolutePath());
		}
	}
}
