// Generated from Expr.g4 by ANTLR 4.9.1
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ExprParser}.
 */
public interface ExprListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ExprParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterProg(ExprParser.ProgContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitProg(ExprParser.ProgContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#instruction}.
	 * @param ctx the parse tree
	 */
	void enterInstruction(ExprParser.InstructionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#instruction}.
	 * @param ctx the parse tree
	 */
	void exitInstruction(ExprParser.InstructionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#type_dec}.
	 * @param ctx the parse tree
	 */
	void enterType_dec(ExprParser.Type_decContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#type_dec}.
	 * @param ctx the parse tree
	 */
	void exitType_dec(ExprParser.Type_decContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#type_def}.
	 * @param ctx the parse tree
	 */
	void enterType_def(ExprParser.Type_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#type_def}.
	 * @param ctx the parse tree
	 */
	void exitType_def(ExprParser.Type_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#type_de_base}.
	 * @param ctx the parse tree
	 */
	void enterType_de_base(ExprParser.Type_de_baseContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#type_de_base}.
	 * @param ctx the parse tree
	 */
	void exitType_de_base(ExprParser.Type_de_baseContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#type_primitif}.
	 * @param ctx the parse tree
	 */
	void enterType_primitif(ExprParser.Type_primitifContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#type_primitif}.
	 * @param ctx the parse tree
	 */
	void exitType_primitif(ExprParser.Type_primitifContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#ordre}.
	 * @param ctx the parse tree
	 */
	void enterOrdre(ExprParser.OrdreContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#ordre}.
	 * @param ctx the parse tree
	 */
	void exitOrdre(ExprParser.OrdreContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#representation}.
	 * @param ctx the parse tree
	 */
	void enterRepresentation(ExprParser.RepresentationContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#representation}.
	 * @param ctx the parse tree
	 */
	void exitRepresentation(ExprParser.RepresentationContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#type_derive}.
	 * @param ctx the parse tree
	 */
	void enterType_derive(ExprParser.Type_deriveContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#type_derive}.
	 * @param ctx the parse tree
	 */
	void exitType_derive(ExprParser.Type_deriveContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#type_id}.
	 * @param ctx the parse tree
	 */
	void enterType_id(ExprParser.Type_idContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#type_id}.
	 * @param ctx the parse tree
	 */
	void exitType_id(ExprParser.Type_idContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#type_construit}.
	 * @param ctx the parse tree
	 */
	void enterType_construit(ExprParser.Type_construitContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#type_construit}.
	 * @param ctx the parse tree
	 */
	void exitType_construit(ExprParser.Type_construitContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#type_ensemble}.
	 * @param ctx the parse tree
	 */
	void enterType_ensemble(ExprParser.Type_ensembleContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#type_ensemble}.
	 * @param ctx the parse tree
	 */
	void exitType_ensemble(ExprParser.Type_ensembleContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#type_intervalle}.
	 * @param ctx the parse tree
	 */
	void enterType_intervalle(ExprParser.Type_intervalleContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#type_intervalle}.
	 * @param ctx the parse tree
	 */
	void exitType_intervalle(ExprParser.Type_intervalleContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#type_ordonne}.
	 * @param ctx the parse tree
	 */
	void enterType_ordonne(ExprParser.Type_ordonneContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#type_ordonne}.
	 * @param ctx the parse tree
	 */
	void exitType_ordonne(ExprParser.Type_ordonneContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#type_non_scalaire}.
	 * @param ctx the parse tree
	 */
	void enterType_non_scalaire(ExprParser.Type_non_scalaireContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#type_non_scalaire}.
	 * @param ctx the parse tree
	 */
	void exitType_non_scalaire(ExprParser.Type_non_scalaireContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#type_tuple}.
	 * @param ctx the parse tree
	 */
	void enterType_tuple(ExprParser.Type_tupleContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#type_tuple}.
	 * @param ctx the parse tree
	 */
	void exitType_tuple(ExprParser.Type_tupleContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#type_relation}.
	 * @param ctx the parse tree
	 */
	void enterType_relation(ExprParser.Type_relationContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#type_relation}.
	 * @param ctx the parse tree
	 */
	void exitType_relation(ExprParser.Type_relationContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#entete_tuple}.
	 * @param ctx the parse tree
	 */
	void enterEntete_tuple(ExprParser.Entete_tupleContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#entete_tuple}.
	 * @param ctx the parse tree
	 */
	void exitEntete_tuple(ExprParser.Entete_tupleContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#attribut_dec}.
	 * @param ctx the parse tree
	 */
	void enterAttribut_dec(ExprParser.Attribut_decContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#attribut_dec}.
	 * @param ctx the parse tree
	 */
	void exitAttribut_dec(ExprParser.Attribut_decContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#attribut_id}.
	 * @param ctx the parse tree
	 */
	void enterAttribut_id(ExprParser.Attribut_idContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#attribut_id}.
	 * @param ctx the parse tree
	 */
	void exitAttribut_id(ExprParser.Attribut_idContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#predicat}.
	 * @param ctx the parse tree
	 */
	void enterPredicat(ExprParser.PredicatContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#predicat}.
	 * @param ctx the parse tree
	 */
	void exitPredicat(ExprParser.PredicatContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#contrainte}.
	 * @param ctx the parse tree
	 */
	void enterContrainte(ExprParser.ContrainteContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#contrainte}.
	 * @param ctx the parse tree
	 */
	void exitContrainte(ExprParser.ContrainteContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#contrainte_liste}.
	 * @param ctx the parse tree
	 */
	void enterContrainte_liste(ExprParser.Contrainte_listeContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#contrainte_liste}.
	 * @param ctx the parse tree
	 */
	void exitContrainte_liste(ExprParser.Contrainte_listeContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#contrainte_dec}.
	 * @param ctx the parse tree
	 */
	void enterContrainte_dec(ExprParser.Contrainte_decContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#contrainte_dec}.
	 * @param ctx the parse tree
	 */
	void exitContrainte_dec(ExprParser.Contrainte_decContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#contrainte_id}.
	 * @param ctx the parse tree
	 */
	void enterContrainte_id(ExprParser.Contrainte_idContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#contrainte_id}.
	 * @param ctx the parse tree
	 */
	void exitContrainte_id(ExprParser.Contrainte_idContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#expression_booleenne}.
	 * @param ctx the parse tree
	 */
	void enterExpression_booleenne(ExprParser.Expression_booleenneContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#expression_booleenne}.
	 * @param ctx the parse tree
	 */
	void exitExpression_booleenne(ExprParser.Expression_booleenneContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#expression_rationnelle}.
	 * @param ctx the parse tree
	 */
	void enterExpression_rationnelle(ExprParser.Expression_rationnelleContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#expression_rationnelle}.
	 * @param ctx the parse tree
	 */
	void exitExpression_rationnelle(ExprParser.Expression_rationnelleContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(ExprParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(ExprParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#calcul}.
	 * @param ctx the parse tree
	 */
	void enterCalcul(ExprParser.CalculContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#calcul}.
	 * @param ctx the parse tree
	 */
	void exitCalcul(ExprParser.CalculContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#identifiant_calcul}.
	 * @param ctx the parse tree
	 */
	void enterIdentifiant_calcul(ExprParser.Identifiant_calculContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#identifiant_calcul}.
	 * @param ctx the parse tree
	 */
	void exitIdentifiant_calcul(ExprParser.Identifiant_calculContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#id_calcul_scalaire}.
	 * @param ctx the parse tree
	 */
	void enterId_calcul_scalaire(ExprParser.Id_calcul_scalaireContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#id_calcul_scalaire}.
	 * @param ctx the parse tree
	 */
	void exitId_calcul_scalaire(ExprParser.Id_calcul_scalaireContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#id_calcul_non_scalaire}.
	 * @param ctx the parse tree
	 */
	void enterId_calcul_non_scalaire(ExprParser.Id_calcul_non_scalaireContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#id_calcul_non_scalaire}.
	 * @param ctx the parse tree
	 */
	void exitId_calcul_non_scalaire(ExprParser.Id_calcul_non_scalaireContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#fonction_appel_calcul}.
	 * @param ctx the parse tree
	 */
	void enterFonction_appel_calcul(ExprParser.Fonction_appel_calculContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#fonction_appel_calcul}.
	 * @param ctx the parse tree
	 */
	void exitFonction_appel_calcul(ExprParser.Fonction_appel_calculContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#fonction_id_calcul}.
	 * @param ctx the parse tree
	 */
	void enterFonction_id_calcul(ExprParser.Fonction_id_calculContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#fonction_id_calcul}.
	 * @param ctx the parse tree
	 */
	void exitFonction_id_calcul(ExprParser.Fonction_id_calculContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#valeur}.
	 * @param ctx the parse tree
	 */
	void enterValeur(ExprParser.ValeurContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#valeur}.
	 * @param ctx the parse tree
	 */
	void exitValeur(ExprParser.ValeurContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#valeur_scalaire}.
	 * @param ctx the parse tree
	 */
	void enterValeur_scalaire(ExprParser.Valeur_scalaireContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#valeur_scalaire}.
	 * @param ctx the parse tree
	 */
	void exitValeur_scalaire(ExprParser.Valeur_scalaireContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#représentation_effective}.
	 * @param ctx the parse tree
	 */
	void enterReprésentation_effective(ExprParser.Représentation_effectiveContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#représentation_effective}.
	 * @param ctx the parse tree
	 */
	void exitReprésentation_effective(ExprParser.Représentation_effectiveContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#valeur_non_scalaire}.
	 * @param ctx the parse tree
	 */
	void enterValeur_non_scalaire(ExprParser.Valeur_non_scalaireContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#valeur_non_scalaire}.
	 * @param ctx the parse tree
	 */
	void exitValeur_non_scalaire(ExprParser.Valeur_non_scalaireContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#valeur_tuple}.
	 * @param ctx the parse tree
	 */
	void enterValeur_tuple(ExprParser.Valeur_tupleContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#valeur_tuple}.
	 * @param ctx the parse tree
	 */
	void exitValeur_tuple(ExprParser.Valeur_tupleContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#valeur_liste_att}.
	 * @param ctx the parse tree
	 */
	void enterValeur_liste_att(ExprParser.Valeur_liste_attContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#valeur_liste_att}.
	 * @param ctx the parse tree
	 */
	void exitValeur_liste_att(ExprParser.Valeur_liste_attContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#valeur_relation}.
	 * @param ctx the parse tree
	 */
	void enterValeur_relation(ExprParser.Valeur_relationContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#valeur_relation}.
	 * @param ctx the parse tree
	 */
	void exitValeur_relation(ExprParser.Valeur_relationContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#valeur_liste_rel}.
	 * @param ctx the parse tree
	 */
	void enterValeur_liste_rel(ExprParser.Valeur_liste_relContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#valeur_liste_rel}.
	 * @param ctx the parse tree
	 */
	void exitValeur_liste_rel(ExprParser.Valeur_liste_relContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#operateur}.
	 * @param ctx the parse tree
	 */
	void enterOperateur(ExprParser.OperateurContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#operateur}.
	 * @param ctx the parse tree
	 */
	void exitOperateur(ExprParser.OperateurContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#operateur_scalaire}.
	 * @param ctx the parse tree
	 */
	void enterOperateur_scalaire(ExprParser.Operateur_scalaireContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#operateur_scalaire}.
	 * @param ctx the parse tree
	 */
	void exitOperateur_scalaire(ExprParser.Operateur_scalaireContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#operateur_non_scalaire}.
	 * @param ctx the parse tree
	 */
	void enterOperateur_non_scalaire(ExprParser.Operateur_non_scalaireContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#operateur_non_scalaire}.
	 * @param ctx the parse tree
	 */
	void exitOperateur_non_scalaire(ExprParser.Operateur_non_scalaireContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#bloc}.
	 * @param ctx the parse tree
	 */
	void enterBloc(ExprParser.BlocContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#bloc}.
	 * @param ctx the parse tree
	 */
	void exitBloc(ExprParser.BlocContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#ifstmt}.
	 * @param ctx the parse tree
	 */
	void enterIfstmt(ExprParser.IfstmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#ifstmt}.
	 * @param ctx the parse tree
	 */
	void exitIfstmt(ExprParser.IfstmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#param_dec}.
	 * @param ctx the parse tree
	 */
	void enterParam_dec(ExprParser.Param_decContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#param_dec}.
	 * @param ctx the parse tree
	 */
	void exitParam_dec(ExprParser.Param_decContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#param_id}.
	 * @param ctx the parse tree
	 */
	void enterParam_id(ExprParser.Param_idContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#param_id}.
	 * @param ctx the parse tree
	 */
	void exitParam_id(ExprParser.Param_idContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#boucle}.
	 * @param ctx the parse tree
	 */
	void enterBoucle(ExprParser.BoucleContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#boucle}.
	 * @param ctx the parse tree
	 */
	void exitBoucle(ExprParser.BoucleContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#fonction_dec}.
	 * @param ctx the parse tree
	 */
	void enterFonction_dec(ExprParser.Fonction_decContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#fonction_dec}.
	 * @param ctx the parse tree
	 */
	void exitFonction_dec(ExprParser.Fonction_decContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#fonction_id}.
	 * @param ctx the parse tree
	 */
	void enterFonction_id(ExprParser.Fonction_idContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#fonction_id}.
	 * @param ctx the parse tree
	 */
	void exitFonction_id(ExprParser.Fonction_idContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#fonction_appel}.
	 * @param ctx the parse tree
	 */
	void enterFonction_appel(ExprParser.Fonction_appelContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#fonction_appel}.
	 * @param ctx the parse tree
	 */
	void exitFonction_appel(ExprParser.Fonction_appelContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#transaction_dec}.
	 * @param ctx the parse tree
	 */
	void enterTransaction_dec(ExprParser.Transaction_decContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#transaction_dec}.
	 * @param ctx the parse tree
	 */
	void exitTransaction_dec(ExprParser.Transaction_decContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#transaction_appel}.
	 * @param ctx the parse tree
	 */
	void enterTransaction_appel(ExprParser.Transaction_appelContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#transaction_appel}.
	 * @param ctx the parse tree
	 */
	void exitTransaction_appel(ExprParser.Transaction_appelContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#transaction_id}.
	 * @param ctx the parse tree
	 */
	void enterTransaction_id(ExprParser.Transaction_idContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#transaction_id}.
	 * @param ctx the parse tree
	 */
	void exitTransaction_id(ExprParser.Transaction_idContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#assertion_dec}.
	 * @param ctx the parse tree
	 */
	void enterAssertion_dec(ExprParser.Assertion_decContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#assertion_dec}.
	 * @param ctx the parse tree
	 */
	void exitAssertion_dec(ExprParser.Assertion_decContext ctx);
	/**
	 * Enter a parse tree produced by {@link ExprParser#assertion_id}.
	 * @param ctx the parse tree
	 */
	void enterAssertion_id(ExprParser.Assertion_idContext ctx);
	/**
	 * Exit a parse tree produced by {@link ExprParser#assertion_id}.
	 * @param ctx the parse tree
	 */
	void exitAssertion_id(ExprParser.Assertion_idContext ctx);
}