

import java.util.ArrayList;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

public class ErreurSyntaxe extends BaseErrorListener {

	/**
	 * Attributes propres
	 */
	private ArrayList<String> erreurs;

	/**
	 * Constructeur
	 */
	public ErreurSyntaxe() {
		erreurs = new ArrayList<>();
	}

	/**
	 * Redéfinir la récupération des erreurs
	 */
	@Override
	public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine,
			String msg, RecognitionException e) {
		//
		// List<String> listeErreur = ((Parser) recognizer).getRuleInvocationStack();
		// Collections.reverse(listeErreur);
		throw new IllegalStateException(
				"Erreur position " + line + ":" + charPositionInLine + " à cause de" + offendingSymbol + " " + msg, e);
	}

	/*
	 * ======================== Set et Get =========================
	 */
	public ArrayList<String> getErreurs() {
		return erreurs;
	}

	public void setErreurs(ArrayList<String> erreurs) {
		this.erreurs = erreurs;
	}

}
