/**
 * 
 */


import java.io.File;
import java.util.Scanner;

/**
 * Classe initiale
 * 
 * @author christina.khnaisser@usherbrooke.ca
 * @since 2021-04-05
 */
public class Mathitis {

	/**
	 * @param args
	 */
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		System.out.println("=========== DEBUT ANALYSE ===========");
		//
		Scanner in = new Scanner(System.in);

		while (true) {
			System.out.println("Entrer le chemin vers un fichier ou !fin pour arrêter le programme: ");
			String chemin = in.next();
			if (chemin.equals("!fin")) {
				System.out.println(">> fin");
				break;
			}
			//
			Analyseur analyseur = new Analyseur();
			analyseur.analyser(new File(chemin));
		}
		//
		// analyseur.analyser(new File(args[0]));
		System.out.println("=========== FIN   ANALYSE ===========");
	}

}
