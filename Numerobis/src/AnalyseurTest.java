
import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class AnalyseurTest {

	private File[] scripts;

	// ***********************************************************************************************
	// Constructeur
	//
	public AnalyseurTest(File repertoire) {
		System.out.println("=========== DEBUT Test ===========");
		scripts = obtenirScript(repertoire);

	}

	@AfterClass
	public static void fin() {
		System.out.println("=========== FIN Test ===========");
	}

	// ***********************************************************************************************
	// Récupération des jeux de données
	//
	/**
	 * Initialisation des jeux de données. Méthode permettant de parcourir les
	 * sous-dossiers du dossier principal (jeu).
	 */
	@Parameterized.Parameters(name = "{index}: Jeu[{0}]={index}")
	public static File[] creationDonneesTest() {
		System.out.println(">> Initialisation des jeux de donn�es");
		String cheminDossierJeu = "jeu";
		File dossierJeu = new File(cheminDossierJeu);
		FileFilter filtreJeu = new FileFilter() {
			@Override
			public boolean accept(File fichier) {
				return !fichier.isHidden() && fichier.isDirectory();
			}
		};
		File[] listeSousDossier = dossierJeu.listFiles(filtreJeu);
		for (File f : listeSousDossier)
			System.out.println(f.getAbsolutePath());
		return listeSousDossier;
	}

	/**
	 * Initialisation des jeux de données. Méthode permettant de parcourir les les
	 * scripts d'essais.
	 */
	private File[] obtenirScript(File repertoire) {
		System.out.println(">> Jeux de données : " + repertoire.getName());
		FileFilter filtreScript = new FileFilter() {
			@Override
			public boolean accept(File fichier) {
				return !fichier.isHidden() && !fichier.isDirectory() && (fichier.getName().endsWith(".txt"));
			}
		};
		File[] scripts = repertoire.listFiles(filtreScript);
		if (scripts == null) {
			scripts = new File[0];
		}
		for (File f : scripts)
			System.out.println(f.getAbsolutePath());
		assertFalse("Aucun script trouv�", scripts.length == 0);
		return scripts;
	}

	// ***********************************************************************************************
	// Cas de test
	//

	@Test
	public void testGrammaire() {
		for (File script : scripts) {
			System.out.println("Analyse de : " + script.getAbsolutePath());
			Analyseur analyseur = new Analyseur();
			analyseur.analyser(script);
		}
	}

}
