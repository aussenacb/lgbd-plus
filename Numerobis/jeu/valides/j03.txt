CREATE TRANSACTION Ajout_Professeur(String : nom, String : _prenom, Bureau : _bureau, Cause : _cause ) 
BEGIN 
	=(matricule, +(lower subtring(nom, 3), +(substring(_prenom, 1), to_char(NOW, 'YYMM'))));
	
	dnom Text;

	 if
	    IN(matriculeP, PROJECTION(matriculeP, RESTRICTION(RELATION PROFESSEUR, ==(matriculep, matricule))));
	  then
	    raise exception
	      'Ajout_Professeur : '
	      'Le professeur est d�j� pr�sent dans la base de donn�es ' ;
	  elsif &&( ==(_bureau, ''),  ==(_cause, ''))
	  then
	  	raise exception
			'Ajout_Professeur : '
			'Le bureau o� la cause non renseign�';
	  elsif &&( !=(_bureau, ''),  !=(_cause, ''))
	  then
	  	raise exception
			'Ajout_Professeur : '
			'Soit le bureau ou la cause doit �tre renseign�, impossible de faire les deux';
	  else
	  	=(dNom, +(_nom, +(' ', _prenom)));
	  	INSERT INTO RELATION professeur TUPLE { matricule, dNom };
		if !=(_bureau, '')
		then
			INSERT INTO RELATION professeur_bureau_pre TUPLE { matricule, _bureau };
		else
			INSERT INTO RELATION professeur_bureau_abs TUPLE { matricule, _cause };
		end if;
	  end if;
	
END;



CREATE TRANSACTION Modifier_Professeur(MatriculeP : _matriculeP, String : _nom) 
BEGIN
  if
    NOT IN(matriculep, PROJECTION(matriculep, RESTRICTION(RELATION professeur, ==(matriculep, _matriculeP))))
  then
    raise exception
      'Modifier_Professeur : '
      'Le professeur %%_matriculeP introuvable dans la base de donn�es' ;
  elsif ==('', _nom)
  then
  	raise exception
		'Modifier_Professeur : '
		'Le nom du professeur ne peut pas �tre vide' ;
  else
  	UPDATE RELATION professeur 
  	=(nom, _nom) 
  	RESTRICTION(RELATION PROFESSEUR, ==(matriculep, _matriculeP));
  end if;
END;



CREATE TRANSACTION Supprimer_Professeur(MatriculeP : _matriculeP) 
BEGIN 
	if
    	NOT IN(matriculep, PROJECTION(matriculep, RESTRICTION(RELATION professeur, ==(matriculep, _matriculeP))))
  	then
    raise exception
      'Supprimer_Professeur : '
      'Le professeur %%_matriculeP n existe pas dans la base de donn�es' ;
  else
  	// le compilateur parcours lui m�me parmi toutes les relations, on enl�ve le cursor de nos requ�te sql
  	DELETE IN(TUPLE { professeurX }, 'DATABASE.RELATIONS') RESTRICTION(matriculep, IN(_matriculeP, 'DATABASE.RELATIONS.TUPLES'));
	//supprime le tuple professeurX inclus dans les relations de la base de donn�es tel que le matricule correspond � celui donn� en param�tre/
	// cela permet une meilleure flexibilit� que les contraintes modifiables en tout temps.
  end if;
END;

CREATE TRANSACTION Attributer_Bureau_Professeur(MatriculeP : _matriculeP, Bureau : _bureau) 
BEGIN
	if NOT IN(matriculep, PROJECTION(matriculep, RESTRICTION(RELATION professeur, ==(matriculep, _matriculeP))))
	then
	raise exception
      'Attribuer_Bureau_Professeur : '
      'Le professeur %%_matriculeP  introuvable dans la base de donn�es';
	else
 	 	if NOT IN(matriculeP, PROJECTION(matriculeP, RESTRICTION(RELATION professeur_bureau_pre, ==(matriculeP, _matriculeP))))
 	 	then
 	 		DELETE professeur_bureau_abs RESTRICTION(matriculep, ==(matriculeP, _matriculeP)) ;
 	 		INSERT INTO RELATION professeur_bureau_pre TUPLE {_matriculeP, _bureau};
 	 	else 
 	 		UPDATE RELATION professeur_bureau_pre =(bureau, _bureau) RESTRICTION(RELATION professeur_bureau_pre, ==(matriculep, _matriculeP)) ;
 	 	end if;
	end if;
 	 
END;

CREATE TRANSACTION Retirer_Bureau_Professeur(MatriculeP : _matriculeP, Cause : _cause) 
BEGIN
	if NOT IN(matriculep, PROJECTION(matriculep, RESTRICTION(RELATION professeur, ==(matriculep, _matriculeP))))
	then
	raise exception
      'Attribuer_Bureau_Professeur : '
      'Le professeur %%_matriculeP  introuvable dans la base de donn�es';
	else
 	 	if NOT IN(matriculeP, PROJECTION(matriculeP, RESTRICTION(RELATION professeur_bureau_abs, ==(matriculeP, _matriculeP))))
 	 	then
 	 		DELETE professeur_bureau_pre RESTRICTION(matriculep, ==(matriculeP, _matriculeP)) ;
 	 		INSERT INTO RELATION professeur_bureau_abs TUPLE {_matriculeP, _cause};
 	 	else 
 	 		UPDATE RELATION professeur_bureau_abs =(cause, _cause) RESTRICTION(RELATION professeur_bureau_abs, ==(matriculep, _matriculeP)) ;
 	 	end if;
	end if;
 	 
END;


fin;
